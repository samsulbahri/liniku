<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Liniku extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['role'])) {
			$_SESSION['role'] = 'umum';
		}

		$this->load->model('hasilTaniModel');
		$this->load->model('agroBisnisModel');
	}

	public function index()
	{
		$data['pesanan'] = '';
		$data['jumlahPesanan'] = '';
		if ($_SESSION['role'] == 'masyarakat') {
			$data['pesanan'] = $this->hasilTaniModel->getAllOrder($_SESSION['id_user']);
			$data['jumlahPesanan'] = $this->hasilTaniModel->getAllOrder($_SESSION['id_user'])->num_rows();
		}
		$this->load->view('template/headerLiniku', $data);
		$this->load->view('landing');
		$this->load->view('template/footerLiniku');
	}

	public function shop()
	{
		$data['JualHasilTani'] = '';
		if ($_SESSION['role'] == 'petani') {
			$data['JualHasilTani'] = $this->hasilTaniModel->getHasilTaniPetani($_SESSION['id_user']);
			$data['satuan'] = $this->hasilTaniModel->getAllSatuan();
		} else if ($_SESSION['role'] == 'masyarakat') {
			$data['JualHasilTani'] = $this->hasilTaniModel->getHasilTaniMasyarakat();
			$data['pesanan'] = $this->hasilTaniModel->getAllOrder($_SESSION['id_user']);
			$data['jumlahPesanan'] = $this->hasilTaniModel->numOrder($_SESSION['id_user']);

			$_SESSION['keranjang'] = array();

			foreach ($data['pesanan']->result() as $pesanan) {
				array_push($_SESSION['keranjang'], $pesanan->id_komoditas);
			}
		} else {
			$data['JualHasilTani'] = $this->hasilTaniModel->getHasilTaniMasyarakat();
		}

		$data['kategori'] = $this->hasilTaniModel->getAllKategori();
		$this->load->view('template/headerLiniku', $data);
		$this->load->view('shop', $data);
		$this->load->view('template/footerLiniku');
	}

	public function sistemPerbenihan()
	{
		if (null !== $this->input->post('search')) {
			$data['benih'] = $this->agroBisnisModel->getSearchPerbenihan($this->input->post('search'))->result();
		} else {
			$data['benih'] = $this->agroBisnisModel->getAllPerbenihan()->result();
		}
		$this->load->view('template/headerLiniku');
		$this->load->view('sistemPerbenihan', $data);
		$this->load->view('template/footerLiniku');
	}

	public function sistemPupuk()
	{
		if (null !== $this->input->post('search')) {
			$data['pupuk'] = $this->agroBisnisModel->getSearchPupuk($this->input->post('search'))->result();
		} else {
			$data['pupuk'] = $this->agroBisnisModel->getAllPupuk()->result();
		}
		$this->load->view('template/headerLiniku');
		$this->load->view('sistemPupuk', $data);
		$this->load->view('template/footerLiniku');
	}

	public function sistemAlat()
	{
		if (null !== $this->input->post('search')) {
			$data['alat'] = $this->agroBisnisModel->getSearchAlat($this->input->post('search'))->result();
		} else {
			$data['alat'] = $this->agroBisnisModel->getAllAlat()->result();
		}
		$this->load->view('template/headerLiniku');
		$this->load->view('sistemAlat', $data);
		$this->load->view('template/footerLiniku');
	}

	public function tambahHasilPertanian()
	{
		$nama = $this->input->post('nama');
		$kategori = $this->input->post('kategori');
		$deskripsi = $this->input->post('deskripsi');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');
		$stok = $this->input->post('stok');

		$namaGambar = $_SESSION['id_user'] . '_' . $nama . '.jpg';
		$namaGambar = str_replace(' ', '_', $namaGambar);
		$fullpath = 'public/images/jual/' . $namaGambar;

		$config['file_name'] 			= $namaGambar;
		$config['upload_path']          = './public/images/jual/';
		$config['allowed_types']        = 'jpg|png|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('gambar')) {
			$_SESSION['notification'] = $this->upload->display_errors();
			redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
		} else {
			$data = array(
				'id_kategori' => $kategori,
				'id_satuan' => $satuan,
				'penjual' => $_SESSION['id_user'],
				'nama_komoditas' => $nama,
				'deskripsi' => $deskripsi,
				'harga' => $harga,
				'stok' => $stok,
				'gambar' => $fullpath
			);

			$this->hasilTaniModel->addOrderKomoditas($data);
			$_SESSION['notification'] = 'berhasilUpload';
			redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
		}
	}

	public function ubahHasilPertanian($idKomoditas)
	{
		$nama = $this->input->post('nama');
		$kategori = $this->input->post('kategori');
		$deskripsi = $this->input->post('deskripsi');
		$harga = $this->input->post('harga');
		$satuan = $this->input->post('satuan');
		$stok = $this->input->post('stok');

		if ($_FILES['gambar']['name'] == '') {
			$data = array(
				'id_kategori' => $kategori,
				'id_satuan' => $satuan,
				'nama_komoditas' => $nama,
				'deskripsi' => $deskripsi,
				'harga' => $harga,
				'stok' => $stok,
			);

			$this->hasilTaniModel->editOrderKomoditas($data, $idKomoditas);
			$_SESSION['notification'] = 'berhasilEdit';
			redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
		} else {
			$data = $this->hasilTaniModel->getSingleHasilTani($idKomoditas);

			unlink('./' . $data->gambar);

			$namaGambar = $_SESSION['id_user'] . '_' . $nama . '.jpg';
			$namaGambar = str_replace(' ', '_', $namaGambar);
			$fullpath = 'public/images/jual/' . $namaGambar;

			$config['file_name'] 			= $namaGambar;
			$config['upload_path']          = './public/images/jual/';
			$config['allowed_types']        = 'jpg|png|jpeg';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('gambar')) {
				$_SESSION['notification'] = $this->upload->display_errors();
				redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
			} else {
				$data = array(
					'id_kategori' => $kategori,
					'id_satuan' => $satuan,
					'nama_komoditas' => $nama,
					'deskripsi' => $deskripsi,
					'harga' => $harga,
					'stok' => $stok,
					'gambar' => $fullpath
				);

				$this->hasilTaniModel->editOrderKomoditas($data, $idKomoditas);
				$_SESSION['notification'] = 'berhasilEdit';
				redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
			}
		}
	}

	public function hapusHasilPertanian($idKomoditas)
	{
		$data = $this->hasilTaniModel->getSingleHasilTani($idKomoditas);

		unlink('./' . $data->gambar);

		$this->hasilTaniModel->deleteOrderKomoditas($idKomoditas);

		$_SESSION['notification'] = 'berhasilHapus';
		redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
	}

	public function tambahPesanan($idKomoditas)
	{
		$banyak = (int)$this->input->post('banyakPesanan');

		$dataKomoditas = $this->hasilTaniModel->getSingleHasilTani($idKomoditas)->row();

		$dataOrder = $this->hasilTaniModel->cekOrder($idKomoditas)->num_rows();

		if ($dataOrder > 0) {
			$dataOrderOld = $this->hasilTaniModel->cekOrder($idKomoditas)->row();
			$jumlahBaru = $dataOrderOld->jumlah + $banyak;
			$dataOrderNew = array(
				'jumlah' => $jumlahBaru,
				'total_harga' => (int)$dataKomoditas->harga * $jumlahBaru
			);

			$this->hasilTaniModel->updateOrder($dataOrderOld->id_order, $dataOrderNew);
		} else {
			$order = array(
				'id_user' => $_SESSION['id_user'],
				'id_komoditas' => $idKomoditas,
				'jumlah' => $banyak,
				'total_harga' => (int)$dataKomoditas->harga * $banyak,
				'status' => 'pending'
			);

			$this->hasilTaniModel->addOrder($order);
		}

		$data = array(
			'stok' => (int)$dataKomoditas->stok - $banyak
		);
		$this->hasilTaniModel->editOrderKomoditas($data, $idKomoditas);

		array_push($_SESSION['keranjang'], $idKomoditas);

		$_SESSION['notification'] = 'berhasilPesan';
		redirect(base_url('agrobisnis/pengelola-hasil-pertanian'));
	}

	public function payOrder()
	{
		$totalHarga = 0;
		$nohp = '';

		$bodyWA = 'Halo! Saya ingin membeli ';
		foreach ($_SESSION['keranjang'] as $keranjang) {
			$dataOrder = $this->hasilTaniModel->cekOrder($keranjang)->row();

			$bodyWA = $bodyWA . $dataOrder->nama_komoditas . ' sebanyak ' . $dataOrder->jumlah . $dataOrder->singkatan_satuan . ', ';

			$totalHarga += $dataOrder->total_harga;

			$nohp = $dataOrder->nohp;

			$this->hasilTaniModel->updateOrder($dataOrder->id_order, array('status' => 'done'));
		}

		$bodyWA = rtrim($bodyWA, ', ') . '. Total harganya adalah Rp ' . number_format($totalHarga, 2, ',', '.') . ' dan saya ingin melakukan pembayaran menggunakan...';

		$bodyWA = str_replace(" ", "%20", $bodyWA);

		redirect('https://api.whatsapp.com/send?phone=' . $nohp . '&text=' . $bodyWA);
	}

	public function sistemHasilPertanian()
	{
		$data['hasilPertanian'] = $this->hasilTaniModel->getAllHasilPertanian()->result();
		$data['sub'] = $this->hasilTaniModel->getAllSubHasilPertanian()->result();
		$this->load->view('template/headerLiniku');
		$this->load->view('sistemHasilPertanian', $data);
		$this->load->view('template/footerLiniku');
	}

	public function lihatHasilPertanian($idSubHasilPertanian)
	{
		$data['hasilPertanian'] = $this->hasilTaniModel->getSubHasilPertanian($idSubHasilPertanian)->row();
		$this->load->view('template/headerLiniku');
		$this->load->view('detailSistemHasilPertanian', $data);
		$this->load->view('template/footerLiniku');
	}
}
