<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (isset($_SESSION['role'])) {
            if ($_SESSION['role'] != 'admin') {
                redirect(base_url('masuk'));
            }
        } else {
            redirect(base_url('masuk'));
        }
        $this->load->model('agrowisataModel');
        $this->load->model('agroBisnisModel');
        $this->load->model('hasilTaniModel');
    }

    public function index()
    {
        redirect(base_url('admin/perbenihan-dan-budidaya'));
    }

    public function perbenihanDanBudidaya()
    {
        $data['benih'] = $this->agroBisnisModel->getAllPerbenihan()->result();
        $this->load->view('template/headerAdmin');
        $this->load->view('admin/perbenihan', $data);
        $this->load->view('template/footerAdmin');
    }

    public function pupukDanPestisida()
    {
        $data['pupuk'] = $this->agroBisnisModel->getAllPupuk()->result();
        $this->load->view('template/headerAdmin');
        $this->load->view('admin/pupuk', $data);
        $this->load->view('template/footerAdmin');
    }

    public function alatDanMesinPertanian()
    {
        $data['alat'] = $this->agroBisnisModel->getAllAlat()->result();
        $this->load->view('template/headerAdmin');
        $this->load->view('admin/mesin', $data);
        $this->load->view('template/footerAdmin');
    }

    public function sistemHasilPertanian()
    {
        $data['main'] = $this->hasilTaniModel->getAllHasilPertanian()->result();
        $data['sub'] = $this->hasilTaniModel->getAllSubHasilPertanian()->result();
        $this->load->view('template/headerAdmin');
        $this->load->view('admin/hasilPertanian', $data);
        $this->load->view('template/footerAdmin');
    }

    public function agrowisata()
    {
        $data['agrowisata'] = $this->agrowisataModel->getAllAgrowisata()->result();
        $data['wahana'] = $this->agrowisataModel->getAllWahanaAndAgrowisata()->result();
        $this->load->view('template/headerAdmin');
        $this->load->view('admin/agrowisata', $data);
        $this->load->view('template/footerAdmin');
    }

    public function tambahAgrowisata()
    {
        $nama = $this->input->post('nama');
        $deskripsi = $this->input->post('deskripsi');
        $alamat = $this->input->post('alamat');
        $hp = $this->input->post('nohp');
        $email = $this->input->post('email');
        $latitude = $this->input->post('latitude');
        $longtitude = $this->input->post('longtitude');

        $namaFoto = $nama . '.jpg';
        $namaFoto = str_replace(' ', '_', $namaFoto);
        $fullpath = 'public/images/agrowisata/' . $namaFoto;

        $config['file_name']             = $namaFoto;
        $config['upload_path']          = './public/images/agrowisata/';
        $config['allowed_types']        = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('gambar')) {
            $_SESSION['notification'] = $this->upload->display_errors();
            redirect(base_url('admin/agrowisata'));
        } else {
            $data = array(
                'nama_agrowisata' => $nama,
                'deskripsi_agrowisata' => $deskripsi,
                'foto_agrowisata' => $fullpath,
                'alamat_agrowisata' => $alamat,
                'phone_agrowisata' => $hp,
                'email_agrowisata' => $email,
                'latitude' => $latitude,
                'longtitude' => $longtitude
            );

            $this->agrowisataModel->tambahAgrowisata($data);
            $_SESSION['notification'] = 'berhasilUpload';
            redirect(base_url('admin/agrowisata'));
        }
    }

    public function ubahAgrowisata($idAgrowisata)
    {
        $nama = $this->input->post('nama');
        $deskripsi = $this->input->post('deskripsi');
        $alamat = $this->input->post('alamat');
        $hp = $this->input->post('nohp');
        $email = $this->input->post('email');
        $latitude = $this->input->post('latitude');
        $longtitude = $this->input->post('longtitude');

        if ($_FILES['gambar']['name'] == '') {
            $data = array(
                'nama_agrowisata' => $nama,
                'deskripsi_agrowisata' => $deskripsi,
                'alamat_agrowisata' => $alamat,
                'phone_agrowisata' => $hp,
                'email_agrowisata' => $email,
                'latitude' => $latitude,
                'longtitude' => $longtitude
            );

            $this->agrowisataModel->editAgrowisata($data, $idAgrowisata);
            $_SESSION['notification'] = 'berhasilEdit';
            redirect(base_url('admin/agrowisata'));
        } else {
            $dataLama = $this->agrowisataModel->getAgrowisata($idAgrowisata)->row();

            unlink('./' . $dataLama->foto_agrowisata);

            $namaFoto = $nama . '.jpg';
            $namaFoto = str_replace(' ', '_', $namaFoto);
            $fullpath = 'public/images/agrowisata/' . $namaFoto;

            $config['file_name']             = $namaFoto;
            $config['upload_path']          = './public/images/agrowisata/';
            $config['allowed_types']        = 'jpg|png|jpeg';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('gambar')) {
                $_SESSION['notification'] = $this->upload->display_errors();
                redirect(base_url('admin/agrowisata'));
            } else {
                $data = array(
                    'nama_agrowisata' => $nama,
                    'deskripsi_agrowisata' => $deskripsi,
                    'foto_agrowisata' => $fullpath,
                    'alamat_agrowisata' => $alamat,
                    'phone_agrowisata' => $hp,
                    'email_agrowisata' => $email,
                    'latitude' => $latitude,
                    'longtitude' => $longtitude
                );

                $this->agrowisataModel->editAgrowisata($data, $idAgrowisata);
                $_SESSION['notification'] = 'berhasilEdit';
                redirect(base_url('admin/agrowisata'));
            }
        }
    }

    public function HapusAgrowisata($idAgrowisata)
    {
        $dataLama = $this->agrowisataModel->getAgrowisata($idAgrowisata)->row();

        unlink('./' . $dataLama->foto_agrowisata);

        $this->agrowisataModel->HapusAgrowisata($idAgrowisata);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/agrowisata'));
    }

    public function tambahWahana()
    {
        $data = array(
            'id_agrowisata' => $this->input->post('nama_agrowisata'),
            'nama_wahana' => $this->input->post('nama_wahana'),
            'deskripsi_wahana' => $this->input->post('deskripsi'),
            'durasi_harga' => $this->input->post('durasi')
        );

        $this->agrowisataModel->tambahWahana($data);
        $_SESSION['notification'] = 'berhasilUpload';
        redirect(base_url('admin/agrowisata'));
    }

    public function ubahWahana($idWahana)
    {
        $data = array(
            'id_agrowisata' => $this->input->post('nama_agrowisata'),
            'nama_wahana' => $this->input->post('nama_wahana'),
            'deskripsi_wahana' => $this->input->post('deskripsi'),
            'durasi_harga' => $this->input->post('durasi')
        );

        $this->agrowisataModel->editWahana($data, $idWahana);
        $_SESSION['notification'] = 'berhasilEdit';
        redirect(base_url('admin/agrowisata'));
    }

    public function hapusWahana($idWahana)
    {
        $this->agrowisataModel->HapusWahana($idWahana);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/agrowisata'));
    }

    public function tambahHasilPertanian()
    {
        $nama = $this->input->post('nama');

        $namaFoto = $nama . '.jpg';
        $namaFoto = str_replace(' ', '_', $namaFoto);
        $fullpath = 'public/images/hasilPertanian/' . $namaFoto;

        $config['file_name']             = $namaFoto;
        $config['upload_path']          = './public/images/hasilPertanian/';
        $config['allowed_types']        = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('gambar')) {
            $_SESSION['notification'] = $this->upload->display_errors();
            redirect(base_url('admin/sistem-hasil-pertanian/'));
        } else {
            $data = array(
                'nama_hasil_pertanian' => $nama,
                'foto_hasil_pertanian' => $fullpath
            );

            $this->hasilTaniModel->tambahHasilPertanian($data);
            $_SESSION['notification'] = 'berhasilUpload';
            redirect(base_url('admin/sistem-hasil-pertanian/'));
        }
    }

    public function ubahHasilPertanian($idHasilTani)
    {
        $nama = $this->input->post('nama');

        if ($_FILES['gambar']['name'] == '') {
            $data = array(
                'nama_hasil_pertanian' => $nama
            );

            $this->hasilTaniModel->ubahHasilPertanian($idHasilTani, $data);
            $_SESSION['notification'] = 'berhasilEdit';
            redirect(base_url('admin/sistem-hasil-pertanian/'));
        } else {
            $dataLama = $this->hasilTaniModel->getHasilPertanian($idHasilTani)->row();

            unlink('./' . $dataLama->foto_hasil_pertanian);

            $namaFoto = $nama . '.jpg';
            $namaFoto = str_replace(' ', '_', $namaFoto);
            $fullpath = 'public/images/hasilPertanian/' . $namaFoto;

            $config['file_name']             = $namaFoto;
            $config['upload_path']          = './public/images/hasilPertanian/';
            $config['allowed_types']        = 'jpg|png|jpeg';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('gambar')) {
                $_SESSION['notification'] = $this->upload->display_errors();
                redirect(base_url('admin/sistem-hasil-pertanian/'));
            } else {
                $data = array(
                    'nama_hasil_pertanian' => $nama,
                    'foto_hasil_pertanian' => $fullpath
                );

                $this->hasilTaniModel->ubahHasilPertanian($idHasilTani, $data);
                $_SESSION['notification'] = 'berhasilEdit';
                redirect(base_url('admin/sistem-hasil-pertanian/'));
            }
        }
    }

    public function hapusHasilPertanian($idHasilTani)
    {
        $dataLama = $this->hasilTaniModel->getHasilPertanian($idHasilTani)->row();

        unlink('./' . $dataLama->foto_hasil_pertanian);

        $this->hasilTaniModel->hapusHasilPertanian($idHasilTani);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/sistem-hasil-pertanian/'));
    }

    public function tambahSubHasilPertanian()
    {
        $namaMain = $this->input->post('nama_main');
        $namaSub = $this->input->post('nama_sub');
        $umur = $this->input->post('umur');
        $tahun = $this->input->post('tahun');
        $keterangan = $this->input->post('keterangan');
        $status = $this->input->post('status');
        $kontak = $this->input->post('kontak');

        $namaFoto = $namaSub . '.jpg';
        $namaFoto = str_replace(' ', '_', $namaFoto);
        $fullpath = 'public/images/hasilPertanian/sub/' . $namaFoto;

        $config['file_name']             = $namaFoto;
        $config['upload_path']          = './public/images/hasilPertanian/sub/';
        $config['allowed_types']        = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('gambar')) {
            $_SESSION['notification'] = $this->upload->display_errors();
            redirect(base_url('admin/sistem-hasil-pertanian/'));
        } else {
            $data = array(
                'id_hasil_pertanian ' => $namaMain,
                'nama_hasil_pertanian_sub' => $namaSub,
                'tahun_hasil_pertanian_sub' => $tahun,
                'umur_hasil_pertanian_sub' => $umur,
                'keterangan_hasil_pertanian_sub' => $keterangan,
                'status_hasil_pertanian_sub' => $status,
                'kontak_hasil_pertanian_sub' => $kontak,
                'foto' => $fullpath,
            );

            $this->hasilTaniModel->tambahSubHasilPertanian($data);
            $_SESSION['notification'] = 'berhasilUpload';
            redirect(base_url('admin/sistem-hasil-pertanian/'));
        }
    }

    public function ubahSubHasilPertanian($idSub)
    {
        $namaMain = $this->input->post('nama_main');
        $namaSub = $this->input->post('nama_sub');
        $umur = $this->input->post('umur');
        $tahun = $this->input->post('tahun');
        $keterangan = $this->input->post('keterangan');
        $status = $this->input->post('status');
        $kontak = $this->input->post('kontak');

        if ($_FILES['gambar']['name'] == '') {
            $data = array(
                'id_hasil_pertanian ' => $namaMain,
                'nama_hasil_pertanian_sub' => $namaSub,
                'tahun_hasil_pertanian_sub' => $tahun,
                'umur_hasil_pertanian_sub' => $umur,
                'keterangan_hasil_pertanian_sub' => $keterangan,
                'status_hasil_pertanian_sub' => $status,
                'kontak_hasil_pertanian_sub' => $kontak,
            );

            $this->hasilTaniModel->ubahSubHasilPertanian($idSub, $data);
            $_SESSION['notification'] = 'berhasilEdit';
            redirect(base_url('admin/sistem-hasil-pertanian/'));
        } else {
            $dataLama = $this->hasilTaniModel->getSubHasilPertanian($idSub)->row();
            unlink('./' . $dataLama->foto);

            $namaFoto = $namaSub . '.jpg';
            $namaFoto = str_replace(' ', '_', $namaFoto);
            $fullpath = 'public/images/hasilPertanian/sub/' . $namaFoto;

            $config['file_name']             = $namaFoto;
            $config['upload_path']          = './public/images/hasilPertanian/sub/';
            $config['allowed_types']        = 'jpg|png|jpeg';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('gambar')) {
                $_SESSION['notification'] = $this->upload->display_errors();
                redirect(base_url('admin/sistem-hasil-pertanian/'));
            } else {
                $data = array(
                    'id_hasil_pertanian ' => $namaMain,
                    'nama_hasil_pertanian_sub' => $namaSub,
                    'tahun_hasil_pertanian_sub' => $tahun,
                    'umur_hasil_pertanian_sub' => $umur,
                    'keterangan_hasil_pertanian_sub' => $keterangan,
                    'status_hasil_pertanian_sub' => $status,
                    'kontak_hasil_pertanian_sub' => $kontak,
                    'foto' => $fullpath,
                );

                $this->hasilTaniModel->ubahSubHasilPertanian($idSub, $data);
                $_SESSION['notification'] = 'berhasilEdit';
                redirect(base_url('admin/sistem-hasil-pertanian/'));
            }
        }
    }

    public function hapusSubHasilPertanian($idSub)
    {
        $dataLama = $this->hasilTaniModel->getSubHasilPertanian($idSub)->row();
        unlink('./' . $dataLama->foto);

        $this->hasilTaniModel->hapusSubHasilPertanian($idSub);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/sistem-hasil-pertanian/'));
    }

    public function tambahPerbenihan()
    {
        $data = array(
            'nama_perbenihan' => $this->input->post('nama'),
            'deskripsi_perbenihan' => $this->input->post('deskripsi')
        );

        $this->agroBisnisModel->tambahPerbenihan($data);
        $_SESSION['notification'] = 'berhasilUpload';
        redirect(base_url('admin/perbenihan-dan-budidaya'));
    }

    public function tambahPupuk()
    {
        $data = array(
            'nama_pupuk' => $this->input->post('nama'),
            'deskripsi_pupuk' => $this->input->post('deskripsi')
        );

        $this->agroBisnisModel->tambahPupuk($data);
        $_SESSION['notification'] = 'berhasilUpload';
        redirect(base_url('admin/pupuk-dan-pestisida'));
    }

    public function tambahAlat()
    {
        $data = array(
            'nama_alat' => $this->input->post('nama'),
            'deskripsi_alat' => $this->input->post('deskripsi')
        );

        $this->agroBisnisModel->tambahAlat($data);
        $_SESSION['notification'] = 'berhasilUpload';
        redirect(base_url('admin/alat-dan-mesin-pertanian'));
    }

    public function ubahPerbenihan($id)
    {
        $data = array(
            'nama_perbenihan' => $this->input->post('nama'),
            'deskripsi_perbenihan' => $this->input->post('deskripsi')
        );

        $this->agroBisnisModel->ubahPerbenihan($id, $data);
        $_SESSION['notification'] = 'berhasilEdit';
        redirect(base_url('admin/perbenihan-dan-budidaya'));
    }

    public function ubahPupuk($id)
    {
        $data = array(
            'nama_pupuk' => $this->input->post('nama'),
            'deskripsi_pupuk' => $this->input->post('deskripsi')
        );

        $this->agroBisnisModel->ubahPupuk($id, $data);
        $_SESSION['notification'] = 'berhasilEdit';
        redirect(base_url('admin/pupuk-dan-pestisida'));
    }

    public function ubahAlat($id)
    {
        $data = array(
            'nama_alat' => $this->input->post('nama'),
            'deskripsi_alat' => $this->input->post('deskripsi')
        );

        $this->agroBisnisModel->ubahAlat($id, $data);
        $_SESSION['notification'] = 'berhasilEdit';
        redirect(base_url('admin/alat-dan-mesin-pertanian'));
    }

    public function hapusPerbenihan($id)
    {
        $this->agroBisnisModel->hapusPerbenihan($id);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/perbenihan-dan-budidaya'));
    }

    public function hapusPupuk($id)
    {
        $this->agroBisnisModel->hapusPupuk($id);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/pupuk-dan-pestisida'));
    }

    public function hapusAlat($id)
    {
        $this->agroBisnisModel->hapusAlat($id);

        $_SESSION['notification'] = 'berhasilHapus';
        redirect(base_url('admin/alat-dan-mesin-pertanian'));
    }
}
