<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agrowisata extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('agrowisataModel');
    }

    public function index()
    {
        $data['agrowisata'] = $this->agrowisataModel->getAllAgrowisata()->result();
        $this->load->view('template/headerLiniku');
        $this->load->view('agrowisata', $data);
        $this->load->view('template/footerLiniku');
    }

    public function lihatAgrowisata($idAgrowisata)
    {
        $data['agrowisata'] = $this->agrowisataModel->getAgrowisata($idAgrowisata)->row();
        $data['wahana'] = $this->agrowisataModel->getAllWahana($idAgrowisata)->result();
        $this->load->view('template/headerLiniku');
        $this->load->view('deskripsi-agrowisata.php', $data);
        $this->load->view('template/footerLiniku');
    }
}
