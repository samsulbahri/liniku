<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('loginModel');
    }

    public function index()
    {
        $data['gagalLogin'] = '';
        if (isset($_SESSION['gagal'])) {
            $data['gagalLogin'] = $_SESSION['gagal'];
            unset($_SESSION['gagal']);
        }
        $this->load->view('login', $data);
    }

    public function masuk()
    {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $where = array(
            'email' => $email,
            'password' => $password
        );

        $cekAkun = $this->loginModel->cekUser("user", $where)->num_rows();

        if ($cekAkun > 0) {
            $dataAkun = $this->loginModel->cekUser("user", $where)->row();

            $akun = array(
                'loggedIn' => TRUE,
                'id_user' => $dataAkun->id_user,
                'nama' => $dataAkun->nama,
                'email' => $email,
                'role' => $dataAkun->role
            );

            $this->session->set_userdata($akun);
            if ($dataAkun->role == 'admin') {
                redirect(base_url('admin/perbenihan-dan-budidaya'));
            } else {
                redirect(base_url());
            }
        } else {
            $_SESSION['gagal'] = "akun tidak ditemukan";
            redirect(base_url('masuk/'));
        }
    }

    public function keluar()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function daftar()
    {
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $nohp = $this->input->post('nohp');
        $role = $this->input->post('role');
        $password1 = md5($this->input->post('password'));
        $password2 = md5($this->input->post('confirmpassword'));

        if ($password1 === $password2) {
            $where = array(
                'email' => $email
            );

            $cekAkun = $this->loginModel->cekUser("user", $where)->num_rows();

            if ($cekAkun == 0) {
                $dataNewUser = array(
                    'nama' => $nama,
                    'email' => $email,
                    'nohp' => $nohp,
                    'password' => $password1,
                    'role' => $role
                );

                $this->loginModel->tambahUser('user', $dataNewUser);

                $dataAkun = $this->loginModel->cekUser("user", array('email' => $email))->row();

                $akun = array(
                    'loggedIn' => TRUE,
                    'id_user' => $dataAkun->id_user,
                    'nama' => $nama,
                    'email' => $email,
                    'role' => $role
                );

                $this->session->set_userdata($akun);

                redirect(base_url());
            } else {
                $_SESSION['gagal'] = "akun sudah ada";
                redirect(base_url('masuk/'));
            }
        } else {
            $_SESSION['gagal'] = "password tidak sama";
            redirect(base_url('masuk/'));
        }
    }
}
