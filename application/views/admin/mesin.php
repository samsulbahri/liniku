<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <?php if (isset($_SESSION['notification'])) {
          if ($_SESSION['notification'] == 'berhasilUpload') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Selamat, anda berhasil menambahkan data.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else if ($_SESSION['notification'] == 'berhasilHapus') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data anda berhasil dihapus.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else if ($_SESSION['notification'] == 'berhasilEdit') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data anda berhasil diubah.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Gagal!</strong> Error: <?= $_SESSION['notification']; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php }
          unset($_SESSION['notification']);
        } ?>
        <div class="card">
          <div class="card-header card-header-info">
            <div class="row">
              <div class="col">
                <h4 class="card-title">Alat dan Mesin Pertanian</h4>
              </div>
              <div class="col">
                <div class="float-right">
                  <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#tambahAlatModal"><i class="material-icons">add</i></button>

                  <div class="modal fade" id="tambahAlatModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title text-dark">Tambah Alat dan Mesin Pertanian</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form action="<?= base_url('admin/alat-dan-mesin-pertanian/tambah-data-alat'); ?>" method="POST">
                            <div class="form-group mb-4">
                              <label>Nama</label>
                              <input type="text" class="form-control" name="nama" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Deskripsi</label>
                              <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-success">Tambah Data</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-info">
                <th>#</th>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th colspan="2" class="text-center">Aksi</th>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($alat as $a) { ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $a->nama_alat; ?></td>
                    <td><?= substr($a->deskripsi_alat, 0, 100) . '...'; ?></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#ubahDataAlatModal<?= $a->id_alat; ?>"><i class="material-icons">edit</i></button></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#hapusDataAlatModal<?= $a->id_alat; ?>"><i class="material-icons">remove_circle_outline</i></button></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>

            <?php foreach ($alat as $a) { ?>
              <div class="modal fade" id="ubahDataAlatModal<?= $a->id_alat; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Ubah Pupuk dan Pestisida</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="<?= base_url('admin/alat-dan-mesin-pertanian/ubah-data-alat/' . $a->id_alat); ?>" method="POST">
                        <div class="form-group mb-4">
                          <label>Nama</label>
                          <input type="text" class="form-control" value="<?= $a->nama_alat; ?>" name="nama" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Deskripsi</label>
                          <textarea name="deskripsi" class="form-control" cols="30" rows="10"><?= $a->deskripsi_alat; ?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-warning">Ubah Data</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="hapusDataAlatModal<?= $a->id_alat; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Hapus Pupuk dan Pestisida</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin ingin menghapus data <?= $a->nama_alat; ?>? Sekali dihapus data ini tidak dapat dikembalikan!
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <a href="<?= base_url('admin/alat-dan-mesin-pertanian/hapus-data-alat/' . $a->id_alat); ?>" class="btn btn-danger">Hapus Data</a>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>