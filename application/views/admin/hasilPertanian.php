<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <?php if (isset($_SESSION['notification'])) {
          if ($_SESSION['notification'] == 'berhasilUpload') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Selamat, anda berhasil menambahkan data.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else if ($_SESSION['notification'] == 'berhasilHapus') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data anda berhasil dihapus.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else if ($_SESSION['notification'] == 'berhasilEdit') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data anda berhasil diubah.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Gagal!</strong> Error: <?= $_SESSION['notification']; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php }
          unset($_SESSION['notification']);
        } ?>
        <div class="card">
          <div class="card-header card-header-rose">
            <div class="row">
              <div class="col">
                <h4 class="card-title">Data Sistem Hasil Pertanian</h4>
              </div>
              <div class="col">
                <div class="float-right">
                  <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#tambahDataHasilPertanianModal"><i class="material-icons">add</i></button>

                  <div class="modal fade" id="tambahDataHasilPertanianModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title text-dark">Tambah Sistem Hasil Pertanian</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form action="<?= base_url('admin/sistem-hasil-pertanian/tambah-data-hasil-pertanian'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group mb-4">
                              <label>Nama Sistem Hasil Pertanian</label>
                              <input type="text" class="form-control" name="nama" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Foto Sistem Hasil Pertanian</label>
                              <input type="file" class="form-control" name="gambar" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-success">Tambah Data</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-rose">
                <th>#</th>
                <th>Foto</th>
                <th>Nama</th>
                <th colspan="2" class="text-center">Aksi</th>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($main as $m) { ?>
                  <tr>
                    <td style="width: 30px;"><?= $no++; ?></td>
                    <td style="width: 200px;"><img src="<?= base_url($m->foto_hasil_pertanian); ?>" width="200"></td>
                    <td><?= $m->nama_hasil_pertanian; ?></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#ubahDataHasilTaniModal<?= $m->id_hasil_pertanian; ?>"><i class="material-icons">edit</i></button></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#hapusDataHasilTaniModal<?= $m->id_hasil_pertanian; ?>"><i class="material-icons">remove_circle_outline</i></button></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php foreach ($main as $m) { ?>
              <div class="modal fade" id="ubahDataHasilTaniModal<?= $m->id_hasil_pertanian; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Ubah Sistem Hasil Pertanian</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="<?= base_url('admin/sistem-hasil-pertanian/ubah-data-hasil-pertanian/' . $m->id_hasil_pertanian); ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group mb-4">
                          <label>Nama Sistem Hasil Pertanian</label>
                          <input type="text" class="form-control" name="nama" value="<?= $m->nama_hasil_pertanian; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Foto Sistem Hasil Pertanian</label>
                          <input type="file" class="form-control" name="gambar">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-warning">Ubah Data</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="hapusDataHasilTaniModal<?= $m->id_hasil_pertanian; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Hapus Sistem Hasil Pertanian</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin ingin menghapus data <?= $m->nama_hasil_pertanian; ?>? Sekali dihapus data ini tidak dapat dikembalikan!
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <a href="<?= base_url('admin/sistem-hasil-pertanian/hapus-data-hasil-pertanian/' . $m->id_hasil_pertanian); ?>" class="btn btn-danger">Hapus Data</a>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-header card-header-primary">
            <div class="row">
              <div class="col">
                <h4 class="card-title">Data Sub Sistem Hasil Pertanian</h4>
              </div>
              <div class="col">
                <div class="float-right">
                  <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#tambahDataSubHasilPertanianModal"><i class="material-icons">add</i></button>

                  <div class="modal fade" id="tambahDataSubHasilPertanianModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title text-dark">Tambah Sub Sistem Hasil Pertanian</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form action="<?= base_url('admin/sistem-hasil-pertanian/tambah-sub-data-hasil-pertanian'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group mb-4">
                              <label>Nama Sistem Hasil Pertanian</label>
                              <select name="nama_main" class="form-control">
                                <?php foreach ($main as $m) { ?>
                                  <option value="<?= $m->id_hasil_pertanian; ?>"><?= $m->nama_hasil_pertanian; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="form-group mb-4">
                              <label>Nama Sub Sistem Hasil Pertanian</label>
                              <input type="text" class="form-control" name="nama_sub" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Umur</label>
                              <input type="text" class="form-control" name="umur" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Tahun</label>
                              <input type="text" class="form-control" name="tahun" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Keterangan</label>
                              <textarea name="keterangan" class="form-control" cols="10" rows="5"></textarea>
                            </div>
                            <div class="form-group mb-4">
                              <label>Status</label>
                              <input type="text" class="form-control" name="status" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Kontak</label>
                              <input type="text" class="form-control" name="kontak" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Foto</label>
                              <input type="file" class="form-control" name="gambar" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-success">Tambah Data</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-rose">
                <th>#</th>
                <th>Nama Hasil Pertanian</th>
                <th>Nama Sub Hasil Pertanian</th>
                <th>Umur</th>
                <th>Tahun</th>
                <th>Status</th>
                <th>Kontak</th>
                <th colspan="2" class="text-center">Aksi</th>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($sub as $s) { ?>
                  <tr>
                    <td style="width: 30px;"><?= $no++; ?></td>
                    <td><?= $s->nama_hasil_pertanian; ?></td>
                    <td><?= $s->nama_hasil_pertanian_sub; ?></td>
                    <td><?= $s->umur_hasil_pertanian_sub; ?></td>
                    <td><?= $s->tahun_hasil_pertanian_sub; ?></td>
                    <td><?= $s->status_hasil_pertanian_sub; ?></td>
                    <td><?= $s->kontak_hasil_pertanian_sub; ?></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#ubahSubDataHasilTaniModal<?= $s->id_hasil_pertanian_sub; ?>"><i class="material-icons">edit</i></button></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#hapusDataHasilTaniModal<?= $s->id_hasil_pertanian_sub; ?>"><i class="material-icons">remove_circle_outline</i></button></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php foreach ($sub as $s) { ?>
              <div class="modal fade" id="ubahSubDataHasilTaniModal<?= $s->id_hasil_pertanian_sub; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Ubah Sistem Hasil Pertanian</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="<?= base_url('admin/sistem-hasil-pertanian/ubah-sub-data-hasil-pertanian/' . $m->id_hasil_pertanian); ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group mb-4">
                          <label>Nama Sistem Hasil Pertanian</label>
                          <select name="nama_main" class="form-control">
                            <?php foreach ($main as $m) {
                              if ($s->id_hasil_pertanian == $m->id_hasil_pertanian) { ?>
                                <option value="<?= $m->id_hasil_pertanian; ?>" selected><?= $m->nama_hasil_pertanian; ?></option>
                              <?php } else { ?>
                                <option value="<?= $m->id_hasil_pertanian; ?>"><?= $m->nama_hasil_pertanian; ?></option>
                              <?php } ?>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="form-group mb-4">
                          <label>Nama Sub Sistem Hasil Pertanian</label>
                          <input type="text" class="form-control" name="nama_sub" value="<?= $s->nama_hasil_pertanian_sub; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Umur</label>
                          <input type="text" class="form-control" value="<?= $s->umur_hasil_pertanian_sub; ?>" name="umur" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Tahun</label>
                          <input type="text" class="form-control" value="<?= $s->tahun_hasil_pertanian_sub; ?>" name="tahun" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Keterangan</label>
                          <textarea name="keterangan" class="form-control" cols="10" rows="5"><?= $s->keterangan_hasil_pertanian_sub; ?></textarea>
                        </div>
                        <div class="form-group mb-4">
                          <label>Status</label>
                          <input type="text" class="form-control" value="<?= $s->status_hasil_pertanian_sub; ?>" name="status" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Kontak</label>
                          <input type="text" class="form-control" value="<?= $s->kontak_hasil_pertanian_sub; ?>" name="kontak" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Foto</label>
                          <input type="file" class="form-control" name="gambar">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-warning">Ubah Data</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="hapusDataHasilTaniModal<?= $s->id_hasil_pertanian_sub; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Hapus Sistem Hasil Pertanian</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin ingin menghapus data <?= $s->nama_hasil_pertanian_sub; ?>? Sekali dihapus data ini tidak dapat dikembalikan!
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <a href="<?= base_url('admin/sistem-hasil-pertanian/hapus-sub-data-hasil-pertanian/' . $s->id_hasil_pertanian_sub); ?>" class="btn btn-danger">Hapus Data</a>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>