<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <?php if (isset($_SESSION['notification'])) {
          if ($_SESSION['notification'] == 'berhasilUpload') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Selamat, anda berhasil menambahkan data.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else if ($_SESSION['notification'] == 'berhasilHapus') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data anda berhasil dihapus.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else if ($_SESSION['notification'] == 'berhasilEdit') { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data anda berhasil diubah.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          <?php } else { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>Gagal!</strong> Error: <?= $_SESSION['notification']; ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <?php }
          unset($_SESSION['notification']);
        } ?>
        <div class="card">
          <div class="card-header card-header-warning">
            <div class="row">
              <div class="col">
                <h4 class="card-title">Data Agrowisata</h4>
              </div>
              <div class="col">
                <div class="float-right">
                  <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#tambahDataAgrowisataModal"><i class="material-icons">add</i></button>

                  <div class="modal fade" id="tambahDataAgrowisataModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title text-dark">Tambah Agrowisata</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form action="<?= base_url('admin/agrowisata/tambah-data-agrowisata'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group mb-4">
                              <label>Nama Agrowisata</label>
                              <input type="text" class="form-control" name="nama" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Deskripsi Agrowisata</label>
                              <textarea class="form-control" name="deskripsi" cols="30" rows="5"></textarea>
                            </div>
                            <div class="form-group mb-4">
                              <label>Alamat Agrowisata</label>
                              <textarea class="form-control" name="alamat" cols="30" rows="5"></textarea>
                            </div>
                            <div class="form-group mb-4">
                              <label>Nomor HP Agrowisata</label>
                              <input type="text" class="form-control" name="nohp" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Email Agrowisata</label>
                              <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Latitude Agrowisata</label>
                              <input type="text" class="form-control" name="latitude" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Longtitude Agrowisata</label>
                              <input type="text" class="form-control" name="longtitude" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Foto Agrowisata</label>
                              <input type="file" class="form-control" name="gambar" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-success">Tambah Data</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-warning">
                <th>#</th>
                <th>Nama</th>
                <th style="width: 200px;">Alamat</th>
                <th>Email</th>
                <th>Latitude</th>
                <th>Longtitude</th>
                <th colspan="2" class="text-center">Aksi</th>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($agrowisata as $a) { ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $a->nama_agrowisata; ?></td>
                    <td><?= substr($a->alamat_agrowisata, 0, 50) . '...'; ?></td>
                    <td><?= $a->email_agrowisata; ?></td>
                    <td><?= $a->latitude; ?></td>
                    <td><?= $a->longtitude; ?></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#ubahDataAgrowisataModal<?= $a->id_agrowisata; ?>"><i class="material-icons">edit</i></button></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#hapusDataAgrowisataModal<?= $a->id_agrowisata; ?>"><i class="material-icons">remove_circle_outline</i></button></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php foreach ($agrowisata as $a) { ?>
              <div class="modal fade" id="ubahDataAgrowisataModal<?= $a->id_agrowisata; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Ubah Agrowisata</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="<?= base_url('admin/agrowisata/ubah-data-agrowisata/' . $a->id_agrowisata); ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group mb-4">
                          <label>Nama Agrowisata</label>
                          <input type="text" class="form-control" name="nama" value="<?= $a->nama_agrowisata; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Deskripsi Agrowisata</label>
                          <textarea class="form-control" name="deskripsi" cols="30" rows="5"><?= $a->deskripsi_agrowisata; ?></textarea>
                        </div>
                        <div class="form-group mb-4">
                          <label>Alamat Agrowisata</label>
                          <textarea class="form-control" name="alamat" cols="30" rows="5"><?= $a->alamat_agrowisata; ?></textarea>
                        </div>
                        <div class="form-group mb-4">
                          <label>Nomor HP Agrowisata</label>
                          <input type="text" class="form-control" name="nohp" value="<?= $a->phone_agrowisata; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Email Agrowisata</label>
                          <input type="email" class="form-control" name="email" value="<?= $a->email_agrowisata; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Latitude Agrowisata</label>
                          <input type="text" class="form-control" name="latitude" value="<?= $a->latitude; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Longtitude Agrowisata</label>
                          <input type="text" class="form-control" name="longtitude" value="<?= $a->longtitude; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Foto Agrowisata</label>
                          <input type="file" class="form-control" name="gambar">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-warning">Ubah Data</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="hapusDataAgrowisataModal<?= $a->id_agrowisata; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Hapus Agrowisata</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin ingin menghapus data <?= $a->nama_agrowisata; ?>? Sekali dihapus data ini tidak dapat dikembalikan!
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <a href="<?= base_url('admin/agrowisata/hapus-data-agrowisata/' . $a->id_agrowisata); ?>" class="btn btn-danger">Hapus Data</a>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-header card-header-success">
            <div class="row">
              <div class="col">
                <h4 class="card-title">Data Wahana</h4>
              </div>
              <div class="col">
                <div class="float-right">
                  <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#tambahDataWahanaModal"><i class="material-icons">add</i></button>

                  <div class="modal fade" id="tambahDataWahanaModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title text-dark">Tambah Wahana</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form action="<?= base_url('admin/agrowisata/tambah-data-wahana'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group mb-4">
                              <label>Nama Agrowisata</label>
                              <select name="nama_agrowisata" class="form-control">
                                <?php foreach ($agrowisata as $a) { ?>
                                  <option value="<?= $a->id_agrowisata; ?>"><?= $a->nama_agrowisata; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="form-group mb-4">
                              <label>Nama Wahana</label>
                              <input type="text" class="form-control" name="nama_wahana" required>
                            </div>
                            <div class="form-group mb-4">
                              <label>Deskripsi Wahana</label>
                              <textarea class="form-control" name="deskripsi" cols="30" rows="5"></textarea>
                            </div>
                            <div class="form-group mb-4">
                              <label>Durasi & Harga</label>
                              <input type="text" class="form-control" name="durasi" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-success">Tambah Data</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-hover">
              <thead class="text-success">
                <th>#</th>
                <th>Nama Wahana</th>
                <th>Nama Agrowisata</th>
                <th style="width: 300px;">Deskripsi</th>
                <th>Durasi & Harga</th>
                <th colspan="2" class="text-center">Aksi</th>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($wahana as $w) { ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $w->nama_wahana; ?></td>
                    <td><?= $w->nama_agrowisata; ?></td>
                    <td><?= substr($w->deskripsi_wahana, 0, 50) . '...'; ?></td>
                    <td><?= $w->durasi_harga; ?></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#ubahDataWahanaModal<?= $w->id_wahana; ?>"><i class="material-icons">edit</i></button></td>
                    <td style="width: 10px;"><button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#hapusDataWahanaModal<?= $w->id_wahana; ?>"><i class="material-icons">remove_circle_outline</i></button></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php foreach ($wahana as $w) { ?>
              <div class="modal fade" id="ubahDataWahanaModal<?= $w->id_wahana; ?>" tabindex=" -1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Ubah Wahana</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="<?= base_url('admin/agrowisata/ubah-data-wahana/' . $w->id_wahana); ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group mb-4">
                          <label>Nama Agrowisata</label>
                          <select name="nama_agrowisata" class="form-control">
                            <?php foreach ($agrowisata as $a) {
                              if ($a->id_agrowisata == $w->id_agrowisata) { ?>
                                <option value="<?= $a->id_agrowisata; ?>" selected><?= $a->nama_agrowisata; ?></option>
                              <?php } else { ?>
                                <option value="<?= $a->id_agrowisata; ?>"><?= $a->nama_agrowisata; ?></option>
                            <?php }
                            } ?>
                          </select>
                        </div>
                        <div class="form-group mb-4">
                          <label>Nama Wahana</label>
                          <input type="text" class="form-control" name="nama_wahana" value="<?= $w->nama_wahana; ?>" required>
                        </div>
                        <div class="form-group mb-4">
                          <label>Deskripsi Wahana</label>
                          <textarea class="form-control" name="deskripsi" cols="30" rows="5"><?= $w->deskripsi_wahana; ?></textarea>
                        </div>
                        <div class="form-group mb-4">
                          <label>Durasi & Harga</label>
                          <input type="text" class="form-control" name="durasi" value="<?= $w->durasi_harga; ?>" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-warning">Ubah Data</button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="hapusDataWahanaModal<?= $w->id_wahana; ?>" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title text-dark">Hapus Wahana</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin ingin menghapus data <?= $w->nama_wahana; ?>? Sekali dihapus data ini tidak dapat dikembalikan!
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <a href="<?= base_url('admin/agrowisata/hapus-data-wahana/' . $w->id_wahana); ?>" class="btn btn-danger">Hapus Data</a>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>