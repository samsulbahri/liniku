    <!-- Start All Title Box -->
    <div class="all-title-box mb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Sistem Pupuk dan Pestisida</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="container mb-5">
        <div class="row mb-4">
            <div class="col">
                <form action="<?= base_url('agrobisnis/sistem-pupuk-dan-pestisida'); ?>" method="POST">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Cari Informasi Sistem Pupuk dan Pestisida disini...">
                        <div class="input-group-append">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php foreach ($pupuk as $p) { ?>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <a href="#" role="button" data-toggle="modal" data-target="#detailPupukModal<?= $p->id_pupuk; ?>">
                        <h1 class="font-weight-bold"><?= $p->nama_pupuk; ?></h1>
                        <p><?= substr($p->deskripsi_pupuk, 0, 100) . '...'; ?></p>
                        <hr>
                    </a>
                </div>
            </div>

            <div class="modal fade" id="detailPupukModal<?= $p->id_pupuk; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $p->nama_pupuk; ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= $p->deskripsi_pupuk; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>