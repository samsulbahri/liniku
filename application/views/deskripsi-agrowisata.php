<div class="container mt-5 mb-5">
    <div class="row">
        <div class="offset-md-1 col-md-10 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 col-xs-12 mb-2">
                            <img src="<?= base_url($agrowisata->foto_agrowisata); ?>" width="200" height="200" class="rounded float-right">
                        </div>

                        <div class="col-md-8 col-xs-12">
                            <div class="full-width">
                                <h2 class="font-weight-bold"><?= $agrowisata->nama_agrowisata; ?></h2>
                                <p><?= $agrowisata->deskripsi_agrowisata; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mb-4">
    <div class="row">
        <div class="col">
            <div class="product-item-filter row">
                <div class="col-12 col-sm-12 text-center">
                    <span class="font-weight-bold text-center">Wahana - Wahana di <?= $agrowisata->nama_agrowisata; ?></span>
                </div>
            </div>

            <div class="product-categorie-box">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="wahana-view">
                        <div class="row">
                            <div class="col">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama Wahana</th>
                                            <th scope="col">Deskripsi</th>
                                            <th scope="col">Durasi dan Biaya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($wahana as $w) { ?>
                                            <tr>
                                                <th scope="row"><?= $no++; ?></th>
                                                <td><?= $w->nama_wahana; ?></td>
                                                <td style="width: 600px;"><?= $w->deskripsi_wahana; ?></td>
                                                <td style="width: 100px;"><?= $w->durasi_harga; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mb-2">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">Kunjungi Kami!</h1>
            <div id="mapid" style="width: 1110px; height: 400px; display: block;"></div>
        </div>
    </div>
</div>

<div class="container mb-5">
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h1>Informasi Agrowisata</h1>
            <p>
                <?= $agrowisata->alamat_agrowisata; ?>
                <br>
                Phone: <?= $agrowisata->phone_agrowisata; ?>
                <br>
                Email: <?= $agrowisata->email_agrowisata; ?>
            </p>
        </div>
    </div>
</div>

<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

<script>
    var map = L.map('mapid', {
        center: [<?= $agrowisata->latitude . ', ' . $agrowisata->longtitude; ?>],
        zoom: 13
    });
    L.marker([-1.083314, 116.955757]).addTo(map)
        .bindPopup("<b><?= $agrowisata->nama_agrowisata; ?>!</b>").openPopup();

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
</script>

<!-- <script>
    CM_ATTR = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://cloudmade.com">CloudMade</a>';

    CM_URL = 'http://{s}.tile.cloudmade.com/d4fc77ea4a63471cab2423e66626cbb6/{styleId}/256/{z}/{x}/{y}.png';

    OSM_URL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    OSM_ATTRIB = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors';
</script> -->

<!-- <script>
    var map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer(CM_URL, {
        attribution: CM_ATTR,
        styleId: 997
    }).addTo(map);



    L.circle([51.508, -0.11], 500, {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5
    }).addTo(map).bindPopup("I am a circle.");

    L.polygon([
        [51.509, -0.08],
        [51.503, -0.06],
        [51.51, -0.047]
    ]).addTo(map).bindPopup("I am a polygon.");
</script> -->