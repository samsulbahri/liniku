    <!-- Start All Title Box -->
    <div class="all-title-box mb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Sistem Hasil Pertanian</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col">
                <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                    <div class="row">
                        <?php foreach ($hasilPertanian as $hasil) { ?>
                            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                <a href="#" role="button" data-toggle="modal" data-target="#subHasilPertanianModal<?= $hasil->id_hasil_pertanian; ?>">
                                    <div class="products-single fix">
                                        <div class="box-img-hover">
                                            <img src="<?= base_url($hasil->foto_hasil_pertanian); ?>" class="img-fluid" alt="Image" />
                                        </div>
                                        <div class="why-text">
                                            <h1 class="text-center font-weight-bold"><?= $hasil->nama_hasil_pertanian; ?> (<?= $hasil->jumlah_sub; ?>)</h1>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="modal fade" id="subHasilPertanianModal<?= $hasil->id_hasil_pertanian; ?>" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"><?= $hasil->nama_hasil_pertanian; ?></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <?php foreach ($sub as $s) {
                                                    if ($s->id_hasil_pertanian == $hasil->id_hasil_pertanian) { ?>
                                                        <div class="col-sm-4 col-md-4 col-lg-3 col-xl-3">
                                                            <a href="<?= base_url('agrobisnis/sistem-hasil-pertanian/lihat-hasil-pertanian/' . $s->id_hasil_pertanian_sub); ?>">
                                                                <div class="products-single fix">
                                                                    <div class="box-img-hover">
                                                                        <img src="<?= base_url($s->foto); ?>" class="img-fluid" />
                                                                    </div>
                                                                    <div class="why-text">
                                                                        <h1 class="text-center font-weight-bold"><?= $s->nama_hasil_pertanian_sub; ?></h1>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                <?php }
                                                } ?>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->