    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Agrowisata</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-sm-12 col-xs-12 shop-content-right">
                    <div class="right-product-box">
                        <?php foreach ($agrowisata as $a) { ?>
                            <div class="list-view-box">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                        <div class="products-single fix">
                                            <div class="box-img-hover">
                                                <img src="<?= base_url($a->foto_agrowisata); ?>" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">
                                        <div class="why-text full-width">
                                            <h4><?= $a->nama_agrowisata; ?></h4>
                                            <p><?= substr($a->deskripsi_agrowisata, 0, 50) . '...'; ?></p>
                                            <a class="btn hvr-hover" href="<?= base_url('agrowisata/lihat-agrowisata/' . $a->id_agrowisata); ?>">Lihat</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>