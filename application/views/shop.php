    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Pengelolaan Hasil Pertanian</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->
    <?php if ($_SESSION['role'] == 'masyarakat' || $_SESSION['role'] == 'umum') { ?>
        <!-- Start Gallery  -->
        <div class="products-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (isset($_SESSION['notification'])) {
                            if ($_SESSION['notification'] == 'berhasilPesan') { ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Berhasil!</strong> Selamat, anda berhasil memesan pengelolaan hasil tani. Anda bisa melihatnya di keranjangku!
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php } else { ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Gagal!</strong> Error: <?= $_SESSION['notification']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        <?php }
                            unset($_SESSION['notification']);
                        } ?>
                        <div class="special-menu text-center">
                            <div class="button-group filter-button-group">
                                <button class="active" data-filter="*">All</button>
                                <?php foreach ($kategori->result() as $k) { ?>
                                    <button class="text-capitalize" data-filter=".<?= $k->nama_kategori; ?>"><?= $k->nama_kategori; ?></button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row special-list">
                    <?php foreach ($JualHasilTani->result() as $jual) { ?>
                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 special-grid <?= $jual->nama_kategori; ?>">
                            <div class="products-single fix">
                                <div class="box-img-hover">
                                    <div class="type-lb">
                                        <p class="sale">Sisa <?= $jual->stok; ?></p>
                                    </div>
                                    <img src="<?= base_url($jual->gambar); ?>" class="img-fluid" style="width: 100%; height: 250px;">
                                    <div class="mask-icon">
                                        <?php if ($_SESSION['role'] == 'umum') { ?>
                                            <a class="cart" data-toggle="modal" data-target="#loginNotificationModal">Tambahkan ke keranjang</a>
                                        <?php } else if ($_SESSION['role'] == 'masyarakat') { ?>
                                            <a class="cart" data-toggle="modal" data-target="#jumlahPesananModal<?= $jual->id_komoditas; ?>">Tambahkan ke keranjang</a>
                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="why-text">
                                    <h4><?= $jual->nama_komoditas; ?></h4>
                                    <h6><?= $jual->deskripsi; ?></h6>
                                    <h5> <?= "Rp " . number_format($jual->harga, 2, ',', '.') . ' / ' . $jual->singkatan_satuan; ?></h5>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Input Jumlah Pesanan -->
                        <div class="modal fade" id="jumlahPesananModal<?= $jual->id_komoditas; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Mau berapa banyak?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?= base_url('agrobisnis/pengelola-hasil-pertanian/tambah-pesanan/' . $jual->id_komoditas); ?>" method="POST">
                                            <div class="row">
                                                <div class="offset-1 col-4 pr-1">
                                                    <input type="range" class="custom-range" id="banyakPesanan<?= $jual->id_komoditas; ?>" name="banyakPesanan" min="1" max="<?= $jual->stok; ?>" value="1" onchange="dynamicValue(this.id)">
                                                </div>
                                                <div class="col-2 pl-0"><span class="valueBanyak">1</span> / Kg</div>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="clearValue()">Tutup</button>
                                        <button type="submit" class="btn btn-success">Pesan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- Modal Notifikasi Login -->
                    <div class="modal fade" id="loginNotificationModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Ups!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Anda Harus Masuk atau daftar terlebih dahulu untuk dapat membeli hasil pertanian di Liniku.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok deh</button>
                                    <a href="<?= base_url("masuk"); ?>" class="btn btn-primary">Masuk Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Gallery  -->
        <script src="<?= base_url('public/js/jquery-3.2.1.min.js'); ?>"></script>
        <script>
            function clearValue() {
                $('.valueBanyak').html('');
            }

            function dynamicValue(id) {
                const $valueSpan = $('.valueBanyak');
                const $value = $('#' + id);
                $valueSpan.html($value.val());
                $value.on('input change', () => {
                    $valueSpan.html($value.val());
                });
            }
        </script>
    <?php } else if ($_SESSION['role'] == 'petani') { ?>
        <!-- Start Gallery  -->
        <div class="products-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (isset($_SESSION['notification'])) {
                            if ($_SESSION['notification'] == 'berhasilUpload') { ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Berhasil!</strong> Selamat, anda berhasil menambahkan pengelolaan hasil tani.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php } else if ($_SESSION['notification'] == 'berhasilHapus') { ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Berhasil!</strong> Data anda berhasil dihapus.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php } else if ($_SESSION['notification'] == 'berhasilEdit') { ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Berhasil!</strong> Data anda berhasil diubah.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php } else { ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Gagal!</strong> Error: <?= $_SESSION['notification']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        <?php }
                            unset($_SESSION['notification']);
                        } ?>
                        <div class="special-menu text-center">
                            <div class="button-group filter-button-group">
                                <button class="active" data-filter="*">Semua Produk</button>
                                <?php foreach ($kategori->result() as $k) { ?>
                                    <button class="text-capitalize" data-filter=".<?= $k->nama_kategori; ?>"><?= $k->nama_kategori; ?></button>
                                <?php } ?>
                            </div>
                            <span data-toggle="modal" data-target="#addDataModal">
                                <a class="btn btn-outline-warning mb-2" style="font-size: 18px;" data-toggle="tooltip" data-placement="right" title="Tambah Hasil Pertanian"><i class="fas fa-plus" style="color: #e1e63e;"></i></a>
                            </span>
                        </div>
                        <!-- Modal Tambah -->
                        <div class="modal fade" id="addDataModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Form Tambah Pengelolaan Hasil Pertanian</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?= base_url('agrobisnis/pengelola-hasil-pertanian/tambah-data'); ?>" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Nama Hasil Pertanian</label>
                                                <input name="nama" type="text" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Kategori Hasil Pertanian</label>
                                                <select name="kategori" class="form-control text-capitalize">
                                                    <?php foreach ($kategori->result() as $k) { ?>
                                                        <option value="<?= $k->id_kategori; ?>"><?= $k->nama_kategori; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Deskripsi Hasil Pertanian</label>
                                                <textarea class="form-control" name="deskripsi" cols="30" rows="4" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Harga Hasil Pertanian</label>
                                                <input name="harga" type="text" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Satuan Hasil Pertanian</label>
                                                <select name="satuan" class="form-control text-capitalized">
                                                    <?php foreach ($satuan->result() as $s) { ?>
                                                        <option value="<?= $s->id_satuan; ?>"><?= $s->nama_satuan . ' (' . $s->singkatan_satuan . ')'; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Stok Hasil Pertanian</label>
                                                <input type="number" name="stok" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Gambar Hasil Pertanian</label>
                                                <input type="file" name="gambar" class="form-control" required>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-success">Tambah Hasil Pertanian</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row special-list">
                    <?php foreach ($JualHasilTani->result() as $jual) { ?>
                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 special-grid <?= $jual->nama_kategori; ?>">
                            <div class="products-single fix">
                                <div class="box-img-hover">
                                    <div class="type-lb">
                                        <p class="sale">Sisa <?= $jual->stok; ?></p>
                                    </div>
                                    <img src="<?= base_url($jual->gambar); ?>" class="img-fluid" style="width: 100%; height: 250px;">
                                    <div class="mask-icon">
                                        <ul>
                                            <li>
                                                <span data-toggle="modal" data-target="#editDataModal<?= $jual->id_komoditas; ?>">
                                                    <a data-toggle="tooltip" data-placement="right" title="Ubah Data"><i class="fas fa-edit" style="color: white;"></i></a>
                                                </span>
                                            </li>
                                            <li>
                                                <span data-toggle="modal" data-target="#deleteDataModal<?= $jual->id_komoditas; ?>">
                                                    <a data-toggle="tooltip" data-placement="right" title="Hapus Data"><i class="fas fa-trash" style="color: white;"></i></a>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="why-text">
                                    <h4><?= $jual->nama_komoditas; ?></h4>
                                    <h6><?= $jual->deskripsi; ?></h6>
                                    <h5> <?= "Rp " . number_format($jual->harga, 2, ',', '.') . ' / ' . $jual->singkatan_satuan; ?></h5>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Ubah -->
                        <div class="modal fade" id="editDataModal<?= $jual->id_komoditas; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Form Ubah Pengelolaan Hasil Pertanian</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?= base_url('agrobisnis/pengelola-hasil-pertanian/ubah-data/' . $jual->id_komoditas); ?>" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Nama Hasil Pertanian</label>
                                                <input name="nama" type="text" class="form-control" value="<?= $jual->nama_komoditas; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Kategori Hasil Pertanian</label>
                                                <select name="kategori" class="form-control text-capitalize">
                                                    <?php foreach ($kategori->result() as $k) { ?>
                                                        <option value="<?= $k->id_kategori; ?>"><?= $k->nama_kategori; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Deskripsi Hasil Pertanian</label>
                                                <textarea class="form-control" name="deskripsi" cols="30" rows="4" required><?= $jual->deskripsi; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Harga Hasil Pertanian</label>
                                                <input name="harga" type="text" class="form-control" value="<?= $jual->harga; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Satuan Hasil Pertanian</label>
                                                <select name="satuan" class="form-control text-capitalized">
                                                    <?php foreach ($satuan->result() as $s) { ?>
                                                        <option value="<?= $s->id_satuan; ?>"><?= $s->nama_satuan . ' (' . $s->singkatan_satuan . ')'; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Stok Hasil Pertanian</label>
                                                <input type="number" name="stok" class="form-control" value="<?= $jual->stok; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Gambar Hasil Pertanian</label>
                                                <input type="file" name="gambar" class="form-control">
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-warning">Ubah Hasil Pertanian</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Hapus -->
                        <div class="modal fade" id="deleteDataModal<?= $jual->id_komoditas; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Konfirmasi Hapus Pengelolaan Hasil Pertanian</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Apakah anda yakin ingin menghapus hasil pertanian ini? Setelah dihapus data ini tidak dapat dikembalikan!</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                        <a href="<?= base_url('agrobisnis/pengelola-hasil-pertanian/hapus-data/' . $jual->id_komoditas); ?>" class="btn btn-danger">Hapus Hasil Pertanian</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- End Gallery  -->
    <?php } ?>