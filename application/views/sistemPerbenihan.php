    <!-- Start All Title Box -->
    <div class="all-title-box mb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Sistem Perbenihan dan Budidaya</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="container">
        <div class="row mb-4">
            <div class="col">
                <form action="<?= base_url('agrobisnis/sistem-perbenihan-dan-budidaya'); ?>" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Cari Informasi Sistem Perbenihan dan Budidaya disini...">
                        <div class="input-group-append">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php foreach ($benih as $b) { ?>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <a href="#" role="button" data-toggle="modal" data-target="#detailPerbenihanModal<?= $b->id_perbenihan; ?>">
                        <h1 class="font-weight-bold"><?= $b->nama_perbenihan; ?></h1>
                        <p><?= substr($b->deskripsi_perbenihan, 0, 100) . '...'; ?></p>
                        <hr>
                    </a>
                </div>
            </div>

            <div class="modal fade" id="detailPerbenihanModal<?= $b->id_perbenihan; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $b->nama_perbenihan; ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= $b->deskripsi_perbenihan; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>