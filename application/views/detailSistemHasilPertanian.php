<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col">
            <div class="text-center mb-2">
                <h1 class="mb-2">
                    <?= $hasilPertanian->nama_hasil_pertanian_sub; ?>
                </h1>
                <img src="<?= base_url($hasilPertanian->foto); ?>" class="rounded img-fluid">
                <hr>
            </div>
            <h3>Komoditas: <?= $hasilPertanian->nama_hasil_pertanian; ?></h3>
            <h3>Tahun: <?= $hasilPertanian->tahun_hasil_pertanian_sub; ?></h3>
            <h3>Umur Tanaman: <?= $hasilPertanian->umur_hasil_pertanian_sub; ?></h3>
            <h3>Keterangan: <?= $hasilPertanian->keterangan_hasil_pertanian_sub; ?></h3>
            <h3>Status: <?= $hasilPertanian->status_hasil_pertanian_sub; ?></h3>
            <h3>Kontak: <?= $hasilPertanian->kontak_hasil_pertanian_sub; ?></h3>

        </div>
    </div>
</div>