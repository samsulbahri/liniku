<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Masuk</title>
    <link rel="stylesheet" href="<?= base_url('public/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css">
    <style>
        body,
        html {

            -webkit-font-smoothing: antialiased;
            text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.01);
            height: 100%;
            margin: 0px;
            padding: 0px;
            line-height: 20px;
            background: -webkit-linear-gradient(90deg, #b0b435 10%, #7adc7d 90%);
            /* Chrome 10+, Saf5.1+ */
            background: -moz-linear-gradient(90deg, #b0b435 10%, #7adc7d 90%);
            /* FF3.6+ */
            background: -ms-linear-gradient(90deg, #b0b435 10%, #7adc7d 90%);
            /* IE10 */
            background: -o-linear-gradient(90deg, #b0b435 10%, #7adc7d 90%);
            /* Opera 11.10+ */
            background: linear-gradient(90deg, #b0b435 10%, #7adc7d 90%);
            /* W3C */

        }

        .about {
            color: #fff;
            text-align: center;
            margin-top: 20px;
        }

        .simplebox,
        .simplebutton {
            outline: none;
            border: 1px solid #ddd !important;
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
            font-size: 12px;
        }

        .container {
            position: relative;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }

        .formHolder {
            width: 300px;
            margin: 0 auto;
            background: rgba(255, 255, 255, 1);
            -webkit-border-radius: 6px;
            border-radius: 6px;
            overflow: hidden;
            padding: 30px;
            box-shadow: 0 4px 2px -2px rgba(0, 0, 0, 0.2);

        }

        .formHolder h3 {
            margin: 0 0 14px 0;
            font-size: 19px;
            font-weight: 600;
        }

        .signUp {
            display: none;
        }

        .login .loginButton {
            margin-right: 5px;
        }

        .signup .loginButton {
            margin-left: 5px;
        }

        .nav a {
            color: #fff;
            text-decoration: none;
            display: inline-block;
            border: 2px solid rgba(255, 255, 255, 0.6);
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 10px 15px 10px 15px;
            letter-spacing: -0.5px;
        }

        .nav span {
            font-style: italic;
            display: inline-block;
            margin: 0 5px 0 5px;
            font-size: 14px;
            color: #eee;
        }

        .signup {
            display: none;
        }

        .page-transition {
            width: 20px;
            height: 20px;
            background: #fff;
            position: fixed;
            top: 50%;
            margin-top: -10px;
            left: 50%;
            z-index: 10000;
            margin-left: -10px;
            -webkit-border-radius: 60px;
            border-radius: 60px;
            display: none;
        }
    </style>
</head>

<body>

    <div class="page-transition"></div>

    <div class="container">

        <div class="formHolder signup">
            <h3 class="text-center">Daftar</h3>

            <form action="<?= base_url('masuk/aksi-daftar'); ?>" method="POST">
                <div class="form-group">
                    <input name="nama" type="text" class="form-control simplebox" placeholder="Nama Lengkap">
                </div>

                <div class="form-group">
                    <input name="email" type="email" class="form-control simplebox" placeholder="Email">
                </div>

                <div class="form-group">
                    <input name="nohp" type="text" name="phone" class="form-control simplebox phone" placeholder="Nomor Handphone" value="+62 ">
                </div>

                <div class="form-group">
                    <input name="password" type="password" class="form-control simplebox" placeholder="Password">
                </div>

                <div class="form-group">
                    <input name="confirmpassword" type="password" class="form-control simplebox" placeholder="Confirm Password">
                </div>

                <div class="form-group">
                    <select name="role" class="form-control simplebox">
                        <option value="masyarakat">Masyarakat</option>
                        <option value="petani">Petani</option>
                    </select>
                </div>
                <div class="text-center">
                    <button type="submit" id="signupButton" class="btn btn-sm btn-success btn-block simplebutton newAccButton">Daftar</button>
                    <br>
                </div>
            </form>

            <div class="text-center">
                <button class="btn btn-sm btn-link loginButton" id="showLogin">Sudah punya akun?</button>
            </div>

        </div>
        <div class="formHolder login">

            <h3 class="text-center">Masuk</h3>

            <form action="<?= base_url('masuk/aksi-masuk'); ?>" method="POST">
                <div class="form-group">
                    <input name="email" type="email" class="form-control simplebox" placeholder="Email" required>
                </div>

                <div class="form-group">
                    <input name="password" type="password" class="form-control simplebox" placeholder="Password" required>
                </div>

                <div class="text-center">
                    <button type="submit" id="loginButton" class="btn btn-sm btn-success btn-block simplebutton loginButton">Login</button>
                    <br>
                </div>
            </form>

            <div class="text-center">
                <button class="btn btn-sm btn-link newAccButton" id="showSignUp">Belum punya akun?</button>
            </div>

        </div>

        <div class="formHolder signUp">

        </div>

    </div>
    <script src="<?= base_url('public/js/jquery-3.2.1.min.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.1/velocity.min.js"></script>
    <script src="https://cdn.jsdelivr.net/velocity/1.2.1/velocity.ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script>
        $('document').ready(function() {
            $('.phone').mask('+62 000 0000 0000');

            <?php if ($gagalLogin == 'akun tidak ditemukan') {
                echo '
                Swal.fire(
                    "Gagal Masuk",
                    "Periksa Username dan Password anda, lalu coba lagi.",
                    "error"
                )
                ';
            } else if ($gagalLogin == 'akun sudah ada') {
                echo '
                Swal.fire(
                    "Gagal Daftar",
                    "Email yang anda masukkan telah terdaftar, silahkan masuk dengan email dan password yang sudah anda daftarkan sebelumnya.",
                    "warning"
                )
                ';
            } else if ($gagalLogin == 'password tidak sama') {
                echo '
                Swal.fire(
                    "Gagal Daftar",
                    "Kedua password tidak sama, coba lagi.",
                    "warning"
                )
                ';
            } ?>

            $('#showLogin').click(function() {

                $(".signup").velocity({
                    scale: "65%",
                    translateZ: 0
                }, {
                    duration: 400,
                    easing: "easeInOutBack",
                    complete: function() {

                        $(".login").velocity({
                            scale: "100%",
                            translateZ: 0
                        }, {
                            duration: 400,
                            easing: "easeInOutBack"
                        });

                        setTimeout(function() {

                            $(".login").velocity("fadeIn", {
                                queue: false,
                                duration: 150
                            });

                        }, 150);


                    }
                });

                setTimeout(function() {

                    $(".signup").velocity("fadeOut", {
                        queue: false,
                        duration: 150
                    });

                }, 150);

            });

            $('#showSignUp').click(function() {

                $(".signup").velocity({
                    scale: "65%",
                    translateZ: 0
                });

                $(".login").velocity({
                    scale: "65%",
                    translateZ: 0
                }, {
                    duration: 400,
                    easing: "easeInOutBack",
                    complete: function() {

                        $(".signup").velocity({
                            scale: "100%",
                            translateZ: 0
                        }, {
                            duration: 400,
                            easing: "easeInOutBack"
                        });

                        setTimeout(function() {

                            $(".signup").velocity("fadeIn", {
                                queue: false,
                                duration: 150
                            });

                        }, 150);


                    }
                });

                setTimeout(function() {

                    $(".login").velocity("fadeOut", {
                        queue: false,
                        duration: 150
                    });

                }, 150);

            });

        });
    </script>
</body>

</html>