    <!-- Start All Title Box -->
    <div class="all-title-box mb-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Sistem Alat dan Mesin Pertanian</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="container">
        <div class="row mb-4">
            <div class="col">
                <form action="<?= base_url('agrobisnis/sistem-alat-dan-mesin-pertanian'); ?>" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Cari Informasi Sistem Alat dan Mesin Pertanian disini...">
                        <div class="input-group-append">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php foreach ($alat as $a) { ?>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <a href="#" role="button" data-toggle="modal" data-target="#detailAlatModal<?= $a->id_alat; ?>">
                        <h1 class="font-weight-bold"><?= $a->nama_alat; ?></h1>
                        <p><?= substr($a->deskripsi_alat, 0, 100) . '...'; ?></p>
                        <hr>
                    </a>
                </div>
            </div>

            <div class="modal fade" id="detailAlatModal<?= $a->id_alat; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $a->nama_alat; ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?= $a->deskripsi_alat; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>