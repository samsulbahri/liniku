<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Admin Liniku
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="<?= base_url('public/admin/css/material-dashboard.css?v=2.1.2'); ?>" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= base_url('public/admin/demo/demo.css'); ?>" rel="stylesheet" />
</head>

<body class="">
    <div class="wrapper ">
        <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <img class="rounded mx-auto d-block" src="<?= base_url('public/images/logo.png'); ?>">
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav mb-5">
                    <li>
                        <p class="text-muted font-weight-normal text-center text-lg">Agrobisnis</p>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('admin/perbenihan-dan-budidaya'); ?>">
                            <i class="material-icons">local_florist</i>
                            <p class="text-wrap">Perbenihan dan Budidaya</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url('admin/pupuk-dan-pestisida'); ?>">
                            <i class="material-icons">eco</i>
                            <p>Pupuk dan Pestisida</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url('admin/alat-dan-mesin-pertanian'); ?>">
                            <i class="material-icons">agriculture</i>
                            <p>Alat dan Mesin Pertanian</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url('admin/sistem-hasil-pertanian'); ?>">
                            <i class="material-icons">shopping_bag</i>
                            <p>Sistem Hasil Pertanian</p>
                        </a>
                    </li>
                    <li>
                        <p class="text-muted font-weight-normal text-center text-lg mt-3">Agrowisata</p>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="<?= base_url('admin/agrowisata'); ?>">
                            <i class="material-icons">toys</i>
                            <p>Agrowisata</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse justify-content-end">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p class="d-lg-none d-md-block">
                                        Account
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                    <a class="dropdown-item" href="<?= base_url("keluar"); ?>">Log out</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->