<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Site Metas -->
    <title>Liniku.id</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Site Icons -->
    <link rel="shortcut icon" href="<?= base_url('public/images/favicon.ico'); ?>" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?= base_url('public/images/apple-touch-icon.png'); ?>" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('public/css/bootstrap.min.css'); ?>" />
    <!-- Site CSS -->
    <link rel="stylesheet" href="<?= base_url('public/css/style.css'); ?>" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="<?= base_url('public/css/responsive.css'); ?>" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?= base_url('public/css/custom.css'); ?>" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />

    <!-- Leaflet -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Start Main Top -->
    <header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="<?= base_url(); ?>"><img src="<?= base_url('public/images/logo.png'); ?>" class="logo" alt="" /></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url(); ?>">Beranda</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle arrow" data-toggle="dropdown">Agrobisnis <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu">
                                <?php if (isset($_SESSION['role'])) {
                                    if ($_SESSION['role'] == 'petani') { ?>
                                        <li><a href="<?= base_url("agrobisnis/sistem-perbenihan-dan-budidaya"); ?>">Sistem Perbenihan dan Budidaya</a></li>
                                        <li><a href="<?= base_url("agrobisnis/sistem-pupuk-dan-pestisida"); ?>">Sistem Pupuk dan Pestisida</a></li>
                                        <li><a href="<?= base_url("agrobisnis/sistem-alat-dan-mesin-pertanian"); ?>">Sistem Alat dan Mesin Pertanian</a></li>
                                        <li><a href="https://katam.litbang.pertanian.go.id/main.aspx" target="_blank">Kalendar Tanam Terpadu</a></li>
                                <?php }
                                } ?>

                                <li><a href="<?= base_url("agrobisnis/sistem-hasil-pertanian"); ?>">Sistem Hasil Pertanian</a></li>
                                <li><a href="https://hargapangan.id" target="_blank">Informasi Harga Pasar</a></li>
                                <li><a href="<?= base_url("agrobisnis/pengelola-hasil-pertanian"); ?>">Pengelola Hasil Pertanian</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url("agrowisata"); ?>">Agrowisata</a>
                        </li>
                        <li class="nav-item">
                            <?php if (isset($_SESSION['loggedIn'])) {
                                echo '<a class="nav-link" href="' . base_url("keluar") . '">Keluar</a>';
                            } else {
                                echo '<a class="nav-link" href="' . base_url("masuk") . '">Masuk</a>';
                            }
                            ?>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->

                <?php if (isset($_SESSION['role'])) {
                    if ($_SESSION['role'] == 'masyarakat') { ?>
                        <!-- Start Atribute Navigation -->
                        <div class="attr-nav">
                            <ul>
                                <li class="side-menu">
                                    <a href="#">
                                        <i class="fa fa-shopping-bag"></i>
                                        <span class="badge"><?= $jumlahPesanan; ?></span>
                                        <p>Keranjangku</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                <?php }
                } ?>
            </div>
            <?php if (isset($_SESSION['role'])) {
                if ($_SESSION['role'] == 'masyarakat') { ?>
                    <!-- Start Side Menu -->
                    <div class="side">
                        <a href="#" class="close-side"><i class="fa fa-times"></i></a>
                        <?php if (count($pesanan->result()) == 0) { ?>
                            <p>Tidak ditemukan Pesanan</p>
                        <?php } else { ?>
                            <li class="cart-box">
                                <ul class="cart-list">
                                    <?php
                                    $totalHarga = 0;
                                    foreach ($pesanan->result() as $p) {
                                        $totalHarga += (int)$p->total_harga;  ?>
                                        <li>
                                            <a href="#" class="photo">
                                                <img src="<?= base_url($p->gambar); ?>" class="cart-thumb" alt="" />
                                            </a>
                                            <h6><a href="#"><?= $p->nama_komoditas; ?> </a></h6>
                                            <p><?= $p->jumlah; ?>x - <span class="price"><?= "Rp " . number_format($p->total_harga, 2, ',', '.'); ?></span></p>
                                        </li>
                                    <?php } ?>
                                    <li class="total">
                                        <a href="<?= base_url('bayar-pesanan'); ?>" class="btn btn-default hvr-hover btn-cart">Bayar</a>
                                        <span class="float-right"><strong>Total</strong>: <?= "Rp " . number_format($totalHarga, 2, ',', '.'); ?></span>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                    </div>
                    <!-- End Side Menu -->
            <?php }
            } ?>
        </nav>
        <!-- End Navigation -->
    </header>
    <!-- End Main Top -->