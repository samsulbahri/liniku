<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'liniku';
$route['agrobisnis/sistem-perbenihan-dan-budidaya'] = 'liniku/sistemPerbenihan';
$route['agrobisnis/sistem-pupuk-dan-pestisida'] = 'liniku/sistemPupuk';
$route['agrobisnis/sistem-alat-dan-mesin-pertanian'] = 'liniku/sistemAlat';
$route['agrobisnis/pengelola-hasil-pertanian'] = 'liniku/shop';
$route['agrobisnis/pengelola-hasil-pertanian/tambah-data'] = 'liniku/tambahHasilPertanian';
$route['agrobisnis/pengelola-hasil-pertanian/tambah-pesanan/(:any)'] = 'liniku/tambahPesanan/$1';
$route['agrobisnis/pengelola-hasil-pertanian/ubah-data/(:any)'] = 'liniku/ubahHasilPertanian/$1';
$route['agrobisnis/pengelola-hasil-pertanian/hapus-data/(:any)'] = 'liniku/hapusHasilPertanian/$1';
$route['agrobisnis/sistem-hasil-pertanian'] = 'liniku/sistemHasilPertanian';
$route['agrobisnis/sistem-hasil-pertanian/lihat-hasil-pertanian/(:any)'] = 'liniku/lihatHasilPertanian/$1';
$route['agrowisata'] = 'agrowisata';
$route['agrowisata/lihat-agrowisata/(:any)'] = 'agrowisata/lihatAgrowisata/$1';
$route['bayar-pesanan'] = 'liniku/payOrder';

// Login
$route['masuk'] = 'login';
$route['masuk/aksi-masuk'] = 'login/masuk';
$route['masuk/aksi-daftar'] = 'login/daftar';
$route['keluar'] = 'login/keluar';

// Admin
$route['admin'] = 'admin';
$route['admin/perbenihan-dan-budidaya'] = 'admin/perbenihanDanBudidaya';
$route['admin/perbenihan-dan-budidaya/tambah-data-perbenihan'] = 'admin/tambahPerbenihan';
$route['admin/perbenihan-dan-budidaya/ubah-data-perbenihan/(:any)'] = 'admin/ubahPerbenihan/$1';
$route['admin/perbenihan-dan-budidaya/hapus-data-perbenihan/(:any)'] = 'admin/hapusPerbenihan/$1';

$route['admin/pupuk-dan-pestisida'] = 'admin/pupukDanPestisida';
$route['admin/pupuk-dan-pestisida/tambah-data-pupuk'] = 'admin/tambahPupuk';
$route['admin/pupuk-dan-pestisida/ubah-data-pupuk/(:any)'] = 'admin/ubahPupuk/$1';
$route['admin/pupuk-dan-pestisida/hapus-data-pupuk/(:any)'] = 'admin/hapusPupuk/$1';

$route['admin/alat-dan-mesin-pertanian'] = 'admin/alatDanMesinPertanian';
$route['admin/alat-dan-mesin-pertanian/tambah-data-alat'] = 'admin/tambahAlat';
$route['admin/alat-dan-mesin-pertanian/ubah-data-alat/(:any)'] = 'admin/ubahAlat/$1';
$route['admin/alat-dan-mesin-pertanian/hapus-data-alat/(:any)'] = 'admin/hapusAlat/$1';

$route['admin/sistem-hasil-pertanian'] = 'admin/sistemHasilPertanian';
$route['admin/sistem-hasil-pertanian/tambah-data-hasil-pertanian'] = 'admin/tambahHasilPertanian';
$route['admin/sistem-hasil-pertanian/ubah-data-hasil-pertanian/(:any)'] = 'admin/ubahHasilPertanian/$1';
$route['admin/sistem-hasil-pertanian/hapus-data-hasil-pertanian/(:any)'] = 'admin/hapusHasilPertanian/$1';
$route['admin/sistem-hasil-pertanian/tambah-sub-data-hasil-pertanian'] = 'admin/tambahSubHasilPertanian';
$route['admin/sistem-hasil-pertanian/ubah-sub-data-hasil-pertanian/(:any)'] = 'admin/ubahSubHasilPertanian/$1';
$route['admin/sistem-hasil-pertanian/hapus-sub-data-hasil-pertanian/(:any)'] = 'admin/hapusSubHasilPertanian/$1';

$route['admin/agrowisata'] = 'admin/agrowisata';
$route['admin/agrowisata/tambah-data-agrowisata'] = 'admin/tambahAgrowisata';
$route['admin/agrowisata/ubah-data-agrowisata/(:any)'] = 'admin/ubahAgrowisata/$1';
$route['admin/agrowisata/hapus-data-agrowisata/(:any)'] = 'admin/hapusAgrowisata/$1';
$route['admin/agrowisata/tambah-data-wahana'] = 'admin/tambahWahana';
$route['admin/agrowisata/ubah-data-wahana/(:any)'] = 'admin/ubahWahana/$1';
$route['admin/agrowisata/hapus-data-wahana/(:any)'] = 'admin/hapusWahana/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
