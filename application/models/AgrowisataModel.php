<?php

class AgrowisataModel extends CI_Model
{
    private string $table = 'agrowisata';

    public function getAllAgrowisata()
    {
        return $this->db->get($this->table);
    }

    public function getAgrowisata($idAgrowisata)
    {
        $this->db->where('id_agrowisata', $idAgrowisata);
        return $this->db->get($this->table);
    }

    public function getAllWahana($idAgrowisata)
    {
        $this->db->where('id_agrowisata', $idAgrowisata);
        return $this->db->get('agrowisata_wahana');
    }

    public function getAllWahanaAndAgrowisata()
    {
        $this->db->select('a.nama_agrowisata, w.id_wahana, w.id_agrowisata, w.nama_wahana, w.deskripsi_wahana, w.durasi_harga')
            ->from($this->table . ' as a, agrowisata_wahana as w')
            ->where('w.id_agrowisata = a.id_agrowisata');

        return $this->db->get();
    }

    public function tambahAgrowisata($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function tambahWahana($data)
    {
        $this->db->insert('agrowisata_wahana', $data);
    }

    public function editAgrowisata($data, $id)
    {
        $this->db->where('id_agrowisata', $id);
        $this->db->update($this->table, $data);
    }

    public function editWahana($data, $id)
    {
        $this->db->where('id_wahana', $id);
        $this->db->update('agrowisata_wahana', $data);
    }

    public function HapusAgrowisata($id)
    {
        $this->db->where('id_agrowisata', $id);
        $this->db->delete($this->table);
    }

    public function hapusWahana($id)
    {
        $this->db->where('id_wahana', $id);
        $this->db->delete('agrowisata_wahana');
    }
}
