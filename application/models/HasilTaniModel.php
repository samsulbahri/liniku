<?php

class HasilTaniModel extends CI_Model
{
    private string $table = 'order_komoditas';

    public function getHasilTaniPetani($idPenjual)
    {
        $this->db->select('k.id_komoditas, k.nama_komoditas, k.deskripsi, k.harga, k.stok, k.gambar, h.nama_kategori, u.id_user, u.nama, s.singkatan_satuan')
            ->from($this->table . ' as k, kategori_hasil_pertanian as h, user as u, satuan as s')
            ->where('k.id_kategori = h.id_kategori')
            ->where('k.id_satuan = s.id_satuan')
            ->where('k.penjual = u.id_user')
            ->where('k.penjual = "' . $idPenjual . '"');

        return $this->db->get();
    }

    public function getHasilTaniMasyarakat()
    {
        // return $this->db->get($this->table);
        $this->db->select('k.id_komoditas, k.nama_komoditas, k.deskripsi, k.harga, k.stok, k.gambar, h.nama_kategori, u.id_user, u.nama, s.singkatan_satuan')
            ->from($this->table . ' as k, kategori_hasil_pertanian as h, user as u, satuan as s')
            ->where('k.id_kategori = h.id_kategori')
            ->where('k.id_satuan = s.id_satuan')
            ->where('k.penjual = u.id_user')
            ->where('k.stok > 0');

        return $this->db->get();
    }

    public function getSingleHasilTani($idKomoditas)
    {
        return $this->db->get_where($this->table, array('id_komoditas' => $idKomoditas));
    }

    public function cekOrder($idKomoditas)
    {
        $this->db->select('o.id_order, o.id_komoditas, o.tanggal_order, o.jumlah, o.total_harga, k.nama_komoditas, k.penjual, k.harga, k.gambar, s.nama_satuan, s.singkatan_satuan, u.nohp')
            ->from('order as o, ' . $this->table . ' as k, satuan as s, user as u')
            ->where('o.id_komoditas = k.id_komoditas')
            ->where('k.id_satuan = s.id_satuan')
            ->where('k.penjual = u.id_user')
            ->where('o.id_komoditas = ' . $idKomoditas)
            ->where('o.status = "pending"');

        return $this->db->get();
        // return $this->db->get_where('order', array('id_komoditas' => $idKomoditas));
    }

    public function updateOrder($idOrder, $data)
    {
        $this->db->where('id_order', $idOrder);
        $this->db->update('order', $data);
    }

    public function getAllKategori()
    {
        return $this->db->get('kategori_hasil_pertanian');
    }

    public function getAllSatuan()
    {
        return $this->db->get('satuan');
    }

    public function getAllOrder($idUser)
    {
        $this->db->select('o.id_order, o.id_komoditas, o.tanggal_order, o.jumlah, o.total_harga, k.nama_komoditas, k.harga, k.gambar')
            ->from('order as o, ' . $this->table . ' as k')
            ->where('o.id_komoditas = k.id_komoditas')
            ->where('o.id_user = ' . $idUser)
            ->where('o.status = "pending"');

        return $this->db->get();
    }
    public function numOrder($idUser)
    {
        return $this->db->get_where('order', array('id_user' => $idUser))->num_rows();
    }

    public function addOrderKomoditas($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function addOrder($data)
    {
        $this->db->insert('order', $data);
    }

    public function editOrderKomoditas($data, $idKomoditas)
    {
        $this->db->where('id_komoditas', $idKomoditas);
        $this->db->update($this->table, $data);
    }

    public function deleteOrderKomoditas($idKomoditas)
    {
        $this->db->where('id_komoditas', $idKomoditas);
        $this->db->delete($this->table);
    }

    public function getAllHasilPertanian()
    {
        $this->db->select('s.id_hasil_pertanian, s.nama_hasil_pertanian, s.foto_hasil_pertanian, (SELECT COUNT(*) FROM hasil_pertanian_sub WHERE hasil_pertanian_sub.id_hasil_pertanian = s.id_hasil_pertanian) AS jumlah_sub')
            ->from('hasil_pertanian as s');
        return $this->db->get();
    }

    public function getAllSubHasilPertanian()
    {
        // return $this->db->get('hasil_pertanian_sub');
        $this->db->select('s.*, h.nama_hasil_pertanian')
            ->from('hasil_pertanian_sub as s, hasil_pertanian as h')
            ->where('s.id_hasil_pertanian = h.id_hasil_pertanian');

        return $this->db->get();
    }

    public function getSubHasilPertanian($id)
    {
        $this->db->select('s.*, h.nama_hasil_pertanian')
            ->from('hasil_pertanian_sub as s, hasil_pertanian as h')
            ->where('s.id_hasil_pertanian = h.id_hasil_pertanian')
            ->where('s.id_hasil_pertanian_sub = ' . $id);

        return $this->db->get();
    }

    public function getHasilPertanian($id)
    {
        return $this->db->get_where('hasil_pertanian', array('id_hasil_pertanian' => $id));
    }

    public function tambahHasilPertanian($data)
    {
        $this->db->insert('hasil_pertanian', $data);
    }

    public function tambahSubHasilPertanian($data)
    {
        $this->db->insert('hasil_pertanian_sub', $data);
    }

    public function ubahHasilPertanian($id, $data)
    {
        $this->db->where('id_hasil_pertanian', $id);
        $this->db->update('hasil_pertanian', $data);
    }

    public function ubahSubHasilPertanian($id, $data)
    {
        $this->db->where('id_hasil_pertanian_sub', $id);
        $this->db->update('hasil_pertanian_sub', $data);
    }

    public function hapusHasilPertanian($id)
    {
        $this->db->where('id_hasil_pertanian', $id);
        $this->db->delete('hasil_pertanian');
    }

    public function hapusSubHasilPertanian($id)
    {
        $this->db->where('id_hasil_pertanian_sub', $id);
        $this->db->delete('hasil_pertanian_sub');
    }
}
