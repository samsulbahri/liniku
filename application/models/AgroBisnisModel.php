<?php

class AgroBisnisModel extends CI_Model
{
    public function getAllPerbenihan()
    {
        return $this->db->get('sistem_perbenihan');
    }

    public function getAllPupuk()
    {
        return $this->db->get('sistem_pupuk');
    }

    public function getAllAlat()
    {
        return $this->db->get('sistem_alat');
    }

    public function getSearchPerbenihan($param)
    {
        return $this->db->like('nama_perbenihan', $param)->get('sistem_perbenihan');
    }

    public function getSearchPupuk($param)
    {
        return $this->db->like('nama_pupuk', $param)->get('sistem_pupuk');
    }

    public function getSearchAlat($param)
    {
        return $this->db->like('nama_alat', $param)->get('sistem_alat');
    }

    public function tambahPerbenihan($data)
    {
        $this->db->insert('sistem_perbenihan', $data);
    }

    public function tambahPupuk($data)
    {
        $this->db->insert('sistem_pupuk', $data);
    }

    public function tambahAlat($data)
    {
        $this->db->insert('sistem_alat', $data);
    }

    public function ubahPerbenihan($id, $data)
    {
        $this->db->where('id_perbenihan', $id);
        $this->db->update('sistem_perbenihan', $data);
    }

    public function ubahPupuk($id, $data)
    {
        $this->db->where('id_pupuk', $id);
        $this->db->update('sistem_pupuk', $data);
    }

    public function ubahAlat($id, $data)
    {
        $this->db->where('id_alat', $id);
        $this->db->update('sistem_alat', $data);
    }

    public function hapusPerbenihan($id)
    {
        $this->db->where('id_perbenihan', $id);
        $this->db->delete('sistem_perbenihan');
    }

    public function hapusPupuk($id)
    {
        $this->db->where('id_pupuk', $id);
        $this->db->delete('sistem_pupuk');
    }

    public function hapusAlat($id)
    {
        $this->db->where('id_alat', $id);
        $this->db->delete('sistem_alat');
    }
}
