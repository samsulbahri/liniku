<?php

class LoginModel extends CI_Model
{
    function cekUser($table, $where)
    {
        return $this->db->get_where($table, $where);
    }

    function tambahUser($table, $data)
    {
        $this->db->insert($table, $data);
    }
}
