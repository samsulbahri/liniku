-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2020 at 08:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_liniku`
--

-- --------------------------------------------------------

--
-- Table structure for table `agrowisata`
--

CREATE TABLE `agrowisata` (
  `id_agrowisata` int(11) NOT NULL,
  `nama_agrowisata` varchar(100) NOT NULL,
  `deskripsi_agrowisata` text NOT NULL,
  `foto_agrowisata` text NOT NULL,
  `alamat_agrowisata` text NOT NULL,
  `phone_agrowisata` varchar(20) NOT NULL,
  `email_agrowisata` varchar(40) NOT NULL,
  `latitude` text NOT NULL,
  `longtitude` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `agrowisata`
--

INSERT INTO `agrowisata` (`id_agrowisata`, `nama_agrowisata`, `deskripsi_agrowisata`, `foto_agrowisata`, `alamat_agrowisata`, `phone_agrowisata`, `email_agrowisata`, `latitude`, `longtitude`) VALUES
(1, 'Lamin Etam Ambors', 'Wahana wisata Lamin Etam Ambors merupakan tempat wisata di balikpapan dimana warga bisa merasakan liburan dengan sepuasnya. Mereka bisa menikmati fasilitas outbond serta fasilitas yang lainnya hanya dalam satu tempat saja. Tempat pariwisata yang berada di Poros Km 28 jalan Balikpapan–Samarinda ini, sungguh mempesona bagi warga yang berkunjung ke wisata ini. Mereka akan disuguhkan oleh pemandangan yang begitu indah. tiket masuk rp.15.000 parkir rp.5.000', 'public/images/agrowisata/Lamin_Etam_Ambors.jpg', 'Jalan Soekarno Hatta Balikpapan Utara, Ambarawang Darat, Kec. Semboja, Kabupaten Kutai Kartanegara, Kalimantan Timur 75271', '+62 895 7052 04722', 'test@gmail.com', '-1.083314', '116.955757');

-- --------------------------------------------------------

--
-- Table structure for table `agrowisata_wahana`
--

CREATE TABLE `agrowisata_wahana` (
  `id_wahana` int(11) NOT NULL,
  `id_agrowisata` int(11) NOT NULL,
  `nama_wahana` varchar(100) NOT NULL,
  `deskripsi_wahana` text NOT NULL,
  `durasi_harga` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `agrowisata_wahana`
--

INSERT INTO `agrowisata_wahana` (`id_wahana`, `id_agrowisata`, `nama_wahana`, `deskripsi_wahana`, `durasi_harga`) VALUES
(1, 1, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa nisi eveniet a similique, consequuntur consequatur ullam perferendis voluptas perspiciatis. Dolorum dolor, harum illum eius, autem magni inventore architecto maxime hic ex sint. Saepe, eaque error aut temporibus in eligendi cumque praesentium debitis, sint aspernatur quos obcaecati laudantium accusamus molestiae similique commodi quia quod officiis voluptate at cupiditate quis quidem quisquam reprehenderit! Dolorum, aut architecto nihil aperiam facere dolor quidem officia placeat molestiae repellat eius itaque nostrum ad officiis ea, quisquam doloribus? Cum harum quam nesciunt labore esse fuga architecto consectetur laudantium perferendis? Dolorum ut quam nesciunt officiis reprehenderit repellat itaque corrupti dolor nam soluta velit eius similique est quos repellendus quo, eos dolore voluptate quia et veritatis voluptatum distinctio consequatur.', '1x / Rp. 20.000,00'),
(2, 1, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa nisi eveniet a similique, consequuntur consequatur ullam perferendis voluptas perspiciatis. Dolorum dolor, harum illum eius, autem magni inventore architecto maxime hic ex sint. Saepe, eaque error aut temporibus in eligendi cumque praesentium debitis, sint aspernatur quos obcaecati laudantium accusamus molestiae similique commodi quia quod officiis voluptate at cupiditate quis quidem quisquam reprehenderit! Dolorum, aut architecto nihil aperiam facere dolor quidem officia placeat molestiae repellat eius itaque nostrum ad officiis ea, quisquam doloribus? Cum harum quam nesciunt labore esse fuga architecto consectetur laudantium perferendis? Dolorum ut quam nesciunt officiis reprehenderit repellat itaque corrupti dolor nam soluta velit eius similique est quos repellendus quo, eos dolore voluptate quia et veritatis voluptatum distinctio consequatur.', '1x / Rp. 20.000,00');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_pertanian`
--

CREATE TABLE `hasil_pertanian` (
  `id_hasil_pertanian` int(11) NOT NULL,
  `nama_hasil_pertanian` varchar(30) NOT NULL,
  `foto_hasil_pertanian` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hasil_pertanian`
--

INSERT INTO `hasil_pertanian` (`id_hasil_pertanian`, `nama_hasil_pertanian`, `foto_hasil_pertanian`) VALUES
(1, 'A', 'public/images/hasilPertanian/A.jpg'),
(2, 'A', 'public/images/hasilPertanian/A.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_pertanian_sub`
--

CREATE TABLE `hasil_pertanian_sub` (
  `id_hasil_pertanian_sub` int(11) NOT NULL,
  `id_hasil_pertanian` int(11) NOT NULL,
  `nama_hasil_pertanian_sub` varchar(30) NOT NULL,
  `tahun_hasil_pertanian_sub` year(4) NOT NULL,
  `umur_hasil_pertanian_sub` varchar(10) NOT NULL,
  `keterangan_hasil_pertanian_sub` text NOT NULL,
  `status_hasil_pertanian_sub` varchar(20) NOT NULL,
  `kontak_hasil_pertanian_sub` varchar(30) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hasil_pertanian_sub`
--

INSERT INTO `hasil_pertanian_sub` (`id_hasil_pertanian_sub`, `id_hasil_pertanian`, `nama_hasil_pertanian_sub`, `tahun_hasil_pertanian_sub`, `umur_hasil_pertanian_sub`, `keterangan_hasil_pertanian_sub`, `status_hasil_pertanian_sub`, `kontak_hasil_pertanian_sub`, `foto`) VALUES
(1, 1, 'B', 2015, '± 125 hari', 'Lorem', 'Lorem', '081277057373', 'public/images/hasilPertanian/sub/A.jpg'),
(2, 1, 'C', 2018, '± 125 hari', 'Lorem Ipsum', 'Dolor', 'Sit Amet', 'public/images/hasilPertanian/sub/Test_Lagi.jpg'),
(3, 2, 'B', 2015, '± 125 hari', 'Lorem', 'Lorem', '081277057373', 'public/images/hasilPertanian/sub/A.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_hasil_pertanian`
--

CREATE TABLE `kategori_hasil_pertanian` (
  `id_kategori` int(100) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori_hasil_pertanian`
--

INSERT INTO `kategori_hasil_pertanian` (`id_kategori`, `nama_kategori`) VALUES
(1, 'buah'),
(2, 'sayuran');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_order` int(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_komoditas` int(11) NOT NULL,
  `tanggal_order` timestamp NOT NULL DEFAULT current_timestamp(),
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `status` enum('pending','done') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id_order`, `id_user`, `id_komoditas`, `tanggal_order`, `jumlah`, `total_harga`, `status`) VALUES
(4, 1, 2, '2020-09-11 07:01:12', 11, 22000, 'done'),
(5, 1, 3, '2020-09-11 07:02:02', 35, 105000, 'done');

-- --------------------------------------------------------

--
-- Table structure for table `order_komoditas`
--

CREATE TABLE `order_komoditas` (
  `id_komoditas` int(100) NOT NULL,
  `id_kategori` int(100) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `penjual` int(11) NOT NULL,
  `nama_komoditas` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(100) NOT NULL,
  `stok` int(100) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_komoditas`
--

INSERT INTO `order_komoditas` (`id_komoditas`, `id_kategori`, `id_satuan`, `penjual`, `nama_komoditas`, `deskripsi`, `harga`, `stok`, `gambar`) VALUES
(2, 1, 1, 2, 'Pepaya', 'pepaya manis', 2000, 64, 'public/images/jual/2_Pepaya.jpg'),
(3, 2, 1, 2, 'Cambah', 'Cambah goreng', 3000, 0, 'public/images/jual/2_Cambah.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(20) NOT NULL,
  `singkatan_satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`, `singkatan_satuan`) VALUES
(1, 'kilogram', 'kg');

-- --------------------------------------------------------

--
-- Table structure for table `sistem_alat`
--

CREATE TABLE `sistem_alat` (
  `id_alat` int(11) NOT NULL,
  `nama_alat` varchar(50) NOT NULL,
  `deskripsi_alat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sistem_alat`
--

INSERT INTO `sistem_alat` (`id_alat`, `nama_alat`, `deskripsi_alat`) VALUES
(1, 'Alat Pengering Berlanjut', 'Alat pengering berlanjut adalah mesin pengering dimana bahan yang akan dikeringkannya dialirkan dengan kecepatan tetap secara terus-menerus melalui udara yang dipanaskan'),
(2, 'Alat Perontok Gabah', 'Alat perontok gabah adalah alat untuk memisahkan butir-butir gabah dari malainya, digerakkan tenaga manusia atau mesin');

-- --------------------------------------------------------

--
-- Table structure for table `sistem_perbenihan`
--

CREATE TABLE `sistem_perbenihan` (
  `id_perbenihan` int(11) NOT NULL,
  `nama_perbenihan` varchar(50) NOT NULL,
  `deskripsi_perbenihan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sistem_perbenihan`
--

INSERT INTO `sistem_perbenihan` (`id_perbenihan`, `nama_perbenihan`, `deskripsi_perbenihan`) VALUES
(1, 'Kelas Benih Padi', 'Secara umum, benih padi yang dikeluarkan Kementerian Pertanian dibagi menjadi 4 kelas, yaitu: 1. Benih Penjenis (BS / Breeder Seed / Label Kuning) 2. Benih Dasar (FS / Foundation Seed / Label putih) 3. Benih Pokok (SS / Stock Seed / Label ungu) 4. Benih Sebar (ES / Extension Seed / Label Biru)'),
(2, 'Fase Pertumbuhan Padi', 'Padi memiliki tiga fase pertumbuhan, yaitu: - fase vegetatif (0-60 hari), - fase generatif (60-90 hari), - dan fase pemasakan (90-120 hari). Sumber: Balai Besar Penelitian Tanaman Padi');

-- --------------------------------------------------------

--
-- Table structure for table `sistem_pupuk`
--

CREATE TABLE `sistem_pupuk` (
  `id_pupuk` int(11) NOT NULL,
  `nama_pupuk` varchar(50) NOT NULL,
  `deskripsi_pupuk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sistem_pupuk`
--

INSERT INTO `sistem_pupuk` (`id_pupuk`, `nama_pupuk`, `deskripsi_pupuk`) VALUES
(1, 'jalur pupuk', 'penempatan pupuk dalam jalur sempit sepanjang deretan tanaman pada jarak tertentu setelah pupuk ditempatkan dalam tanah, biasanya pupuk itu langsung ditimbun dan tidak diaduk dengan tanah\r\n\r\n'),
(2, 'Pupuk organik', 'Pupuk organik adalah pupuk yang berasal dari tumbuhan mati, kotoran hewan dan/atau bagian hewan dan/atau limbah organik lainnya yang telah melalui proses rekayasa, berbentuk padat atau cair, dapat diperkaya dengan bahan mineral dan/atau mikroba, yang bermanfaat untuk meningkatkan kandungan hara dan bahan organik tanah serta memperbaiki sifat fisik, kimia dan biologi tanah');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `nohp` varchar(17) NOT NULL,
  `password` varchar(32) NOT NULL,
  `role` enum('masyarakat','petani','admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `nohp`, `password`, `role`) VALUES
(1, 'Sam', 's@g.c', '081277057373', '1f1770fc0292f85a3933910aa6045bd3', 'masyarakat'),
(2, 'Rafli', 'r@g.c', '+62 812 7705 7373', '1f1770fc0292f85a3933910aa6045bd3', 'petani'),
(3, 'Fikri', 'f@g.c', '081277057375', '1f1770fc0292f85a3933910aa6045bd3', 'admin'),
(4, 'Test', 't@t', '+62 812 7705 7373', '1f1770fc0292f85a3933910aa6045bd3', 'petani');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agrowisata`
--
ALTER TABLE `agrowisata`
  ADD PRIMARY KEY (`id_agrowisata`);

--
-- Indexes for table `agrowisata_wahana`
--
ALTER TABLE `agrowisata_wahana`
  ADD PRIMARY KEY (`id_wahana`),
  ADD KEY `id_agrowisata` (`id_agrowisata`);

--
-- Indexes for table `hasil_pertanian`
--
ALTER TABLE `hasil_pertanian`
  ADD PRIMARY KEY (`id_hasil_pertanian`);

--
-- Indexes for table `hasil_pertanian_sub`
--
ALTER TABLE `hasil_pertanian_sub`
  ADD PRIMARY KEY (`id_hasil_pertanian_sub`),
  ADD KEY `id_hasil_pertanian` (`id_hasil_pertanian`);

--
-- Indexes for table `kategori_hasil_pertanian`
--
ALTER TABLE `kategori_hasil_pertanian`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_komoditas` (`id_komoditas`);

--
-- Indexes for table `order_komoditas`
--
ALTER TABLE `order_komoditas`
  ADD PRIMARY KEY (`id_komoditas`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `penjual` (`penjual`),
  ADD KEY `id_satuan` (`id_satuan`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indexes for table `sistem_alat`
--
ALTER TABLE `sistem_alat`
  ADD PRIMARY KEY (`id_alat`);

--
-- Indexes for table `sistem_perbenihan`
--
ALTER TABLE `sistem_perbenihan`
  ADD PRIMARY KEY (`id_perbenihan`);

--
-- Indexes for table `sistem_pupuk`
--
ALTER TABLE `sistem_pupuk`
  ADD PRIMARY KEY (`id_pupuk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agrowisata`
--
ALTER TABLE `agrowisata`
  MODIFY `id_agrowisata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `agrowisata_wahana`
--
ALTER TABLE `agrowisata_wahana`
  MODIFY `id_wahana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hasil_pertanian`
--
ALTER TABLE `hasil_pertanian`
  MODIFY `id_hasil_pertanian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hasil_pertanian_sub`
--
ALTER TABLE `hasil_pertanian_sub`
  MODIFY `id_hasil_pertanian_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kategori_hasil_pertanian`
--
ALTER TABLE `kategori_hasil_pertanian`
  MODIFY `id_kategori` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order_komoditas`
--
ALTER TABLE `order_komoditas`
  MODIFY `id_komoditas` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sistem_alat`
--
ALTER TABLE `sistem_alat`
  MODIFY `id_alat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sistem_perbenihan`
--
ALTER TABLE `sistem_perbenihan`
  MODIFY `id_perbenihan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sistem_pupuk`
--
ALTER TABLE `sistem_pupuk`
  MODIFY `id_pupuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `agrowisata_wahana`
--
ALTER TABLE `agrowisata_wahana`
  ADD CONSTRAINT `agrowisata_wahana_ibfk_1` FOREIGN KEY (`id_agrowisata`) REFERENCES `agrowisata` (`id_agrowisata`) ON DELETE CASCADE;

--
-- Constraints for table `hasil_pertanian_sub`
--
ALTER TABLE `hasil_pertanian_sub`
  ADD CONSTRAINT `hasil_pertanian_sub_ibfk_1` FOREIGN KEY (`id_hasil_pertanian`) REFERENCES `hasil_pertanian` (`id_hasil_pertanian`) ON DELETE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`id_komoditas`) REFERENCES `order_komoditas` (`id_komoditas`);

--
-- Constraints for table `order_komoditas`
--
ALTER TABLE `order_komoditas`
  ADD CONSTRAINT `order_komoditas_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_hasil_pertanian` (`id_kategori`),
  ADD CONSTRAINT `order_komoditas_ibfk_2` FOREIGN KEY (`penjual`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `order_komoditas_ibfk_3` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id_satuan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
